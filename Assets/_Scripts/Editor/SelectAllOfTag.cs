﻿using System.Collections;
using UnityEditor;
using UnityEngine;

public class SelectAllOfTag : ScriptableWizard
{
    public string searchTag;
    [MenuItem("My tools/Select all of tag...")]
    static void SelectAllOfTagWizard()
    {
        ScriptableWizard.DisplayWizard<SelectAllOfTag>("Select all of tag...", "Make selection");
    }

    private void OnWizardCreate()
    {
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag(searchTag);
        Selection.objects = gameObjects;
    }
}
