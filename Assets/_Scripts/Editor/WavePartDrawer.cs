﻿//using UnityEditor;
//using UnityEditor.UIElements;
//using UnityEngine.UIElements;
//using UnityEngine;

//[CustomPropertyDrawer(typeof(WavePart))]
//public class WavePartDrawer : PropertyDrawer
//{
//    public override VisualElement CreatePropertyGUI(SerializedProperty property)
//    {
//        var container = new VisualElement();

//        var enemyTypeField = new PropertyField(property.FindPropertyRelative("enemyType"));
//        var enemiesNumberFiled = new PropertyField(property.FindPropertyRelative("enemiesNumber"));
//        var spawnerField = new PropertyField(property.FindPropertyRelative("spawner"));
//        var pathField = new PropertyField(property.FindPropertyRelative("path"));
//        var spawningIntervalField = new PropertyField(property.FindPropertyRelative("spawningInterval"));

//        container.Add(enemyTypeField);
//        container.Add(enemiesNumberFiled);
//        container.Add(spawnerField);
//        container.Add(pathField);
//        container.Add(spawningIntervalField);

//        return container;
//    }

//    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
//    {
//        // Using BeginProperty / EndProperty on the parent property means that
//        // prefab override logic works on the entire property.
//        EditorGUI.BeginProperty(position, label, property);

//        // Draw label
//        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

//        // Don't make child fields be indented
//        var indent = EditorGUI.indentLevel;
//        EditorGUI.indentLevel = 0;

//        // Calculate rects
//        var enemyTypeRect = new Rect(position.x, position.y, 50, position.height);
//        var enemiesNumberRect = new Rect(position.x + 35, position.y, 25, position.height);
//        var spawnerRect = new Rect(position.x + 90, position.y, position.width - 90, position.height);
//        var pathRect = new Rect(position.x, position.y + 15, 50, position.height);
//        var spawningIntervalRect = new Rect(position.x + 90, position.y + 15, position.width - 90, position.height);

//        // Draw fields - passs GUIContent.none to each so they are drawn without labels
//        EditorGUI.PropertyField(enemyTypeRect, property.FindPropertyRelative("enemyType"), GUIContent.none);
//        EditorGUI.PropertyField(enemiesNumberRect, property.FindPropertyRelative("enemiesNumber"), GUIContent.none);
//        EditorGUI.PropertyField(spawnerRect, property.FindPropertyRelative("spawner"), GUIContent.none);
//        EditorGUI.PropertyField(pathRect, property.FindPropertyRelative("path"), GUIContent.none);
//        EditorGUI.PropertyField(spawningIntervalRect, property.FindPropertyRelative("spawningInterval"), GUIContent.none);

//        // Set indent back to what it was
//        EditorGUI.indentLevel = indent;

//        EditorGUI.EndProperty();
//    }

//    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//    {
//        return 27;
//    }
//}
