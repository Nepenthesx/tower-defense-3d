﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : Character
{
    #region publiczne zmienne

    public int playerLiveReduceValue = 1;
    public int worthValue = 1;
    public Vector3 pathOffset;

    #endregion

    #region prywatne zmienne

    private PathScript path;
    private int currentPathPointIndex = 0;

    #endregion

    #region publiczne metody

    public void InitializePath(PathScript path)
    {
        if (this.path != null || path == null)
        {
            return;
        }

        this.path = path;
    }

    #endregion

    #region prywatne metody

    protected override void Start()
    {
        base.Start();
        currentCoroutine = StartCoroutine(FollowPath());
    }

    protected override void Update()
    {
        base.Update();
        if (state == CharacterState.Idle && currentPathPointIndex < path.pointList.Count)
        {
            if (currentCoroutine != null)
            {
                StopCoroutine(currentCoroutine);
            }

            currentCoroutine = StartCoroutine(FollowPath());
        }
    }

    protected IEnumerator FollowPath()
    {
        while (currentPathPointIndex < path.pointList.Count)
        {
            Vector3 currentPathPointPosition = path.pointList[currentPathPointIndex].transform.position;

            currentCoroutine = StartCoroutine(WalkTo(currentPathPointPosition));
            yield return currentCoroutine;
            currentPathPointIndex++;
        }

        EventsManager.current.ReducePlayerLive(playerLiveReduceValue);
        StartCoroutine(base.DestroyAfterDeath());
    }

    private void OnTriggerEnter(Collider other)
    {
        DisableTrap(other);
    }

    private void DisableTrap(Collider other)
    {
        if (!other.CompareTag("Trap"))
            return;

        float trapDisablingChance = GetStats()[CharacterStatType.TrapDisablingChance];
        if (trapDisablingChance == 0)
            return;
        
        if (trapDisablingChance < Random.Range(0f, 1f))
            return;
        
        if (other.gameObject.TryGetComponent(out IntervalTrapBehaviour intervalTrap))
        {
            intervalTrap.DisableTrap(stats.GetStats()[CharacterStatType.TrapDisablingTime]);
        } else if (other.gameObject.TryGetComponent(out SpawnTrapBehaviour spawnTrap))
        {
            spawnTrap.DisableTrap(stats.GetStats()[CharacterStatType.TrapDisablingTime]);
        }
    }

    protected override IEnumerator DestroyAfterDeath()
    {
        EventsManager.current.EnemyDead(this);
        return base.DestroyAfterDeath();
    }

    #endregion
}