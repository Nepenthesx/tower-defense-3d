﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject arrowPrefab;
    public float shootSpeed;
    public float range;



    [SerializeField]
    private List<GameObject> enemyList = new List<GameObject>();

    private GameObject newArrow;

    private float nextShootTime = 0;
    
    void Start()
    {
        GetComponent<SphereCollider>().radius = range;
    }

    
    void Update()
    {
        if (enemyList.Count != 0)
        {
            if (Time.time > nextShootTime)
            {
                newArrow = Instantiate(arrowPrefab, new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z), Quaternion.Euler(-90, 0, 0)) as GameObject;
                newArrow.GetComponent<Arrow1>().aim = enemyList[0];

                nextShootTime = Time.time + 3;
            }
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Enemy"))
        {
            enemyList.Add(col.gameObject);
        }
    }
}
