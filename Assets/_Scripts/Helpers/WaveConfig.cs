﻿/// <summary>
/// Zestaw opcji pojedynczej fali przeciwników, która może składać się z różnych składowych
/// </summary>
[System.Serializable]
public class WaveConfig
{
    public float startTime;
    public WavePart[] waveParts;

    public string GenerateString()
    {
        string finalString = "";
        for (int i = 0; i < waveParts.Length; i++)
        {
            WavePart wavePart = waveParts[i];
            finalString += wavePart.enemiesNumber + " " + wavePart.enemyType.name + "\n";
        }
        return finalString;
    }
}
