﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewCharacter", menuName = "Character/New Character")]
public class CharacterType : ScriptableObject
{
    public string typeName;
    public GameObject prefab;
    public Sprite icon;
    public DamageOptions[] attackOptions;
    public DamageOptions[] vulnerabilities;
    // na razie nie umiem inaczej, więc musi być tak:
    [Header("Initial stats")]
    public float health;
    public float moveSpeed;
    public float attackInterval;
    public float luck;
    public float trapDisablingChance;
    public float trapDisablingTime;
}
