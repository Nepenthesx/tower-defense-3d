﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Składowa fali przeciwników. Zawiera typ przeciwnika, gdzie go spawnować, co ile czasu, po jakiej ścieżce ma iść i ilu takich typków ma być.
/// </summary>
[System.Serializable]
public class WavePart
{
    public CharacterType enemyType;
    public int enemiesNumber;
    public Transform spawner;
    public PathScript path;
    public float spawningInterval;
}
