﻿using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Skrypt dołączany całości ścieżki, przekazuje Enemy kolejność punktów do któych ma się poruszać
/// </summary>
public class PathScript : MonoBehaviour
{
    //Kolejność punktów jest odgórne wpisywana w silniku
    public List<GameObject> pointList = new List<GameObject>();
    public Color color;
    public Vector3 pathGizmosOffset;

    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Vector3 prevPointPosition = pointList[0].transform.position;
        for (int i = 0; i < pointList.Count; i++)
        {
            Vector3 currentPointPosition = pointList[i].transform.position;
            Gizmos.DrawSphere(currentPointPosition + pathGizmosOffset, 0.1f);
            if (prevPointPosition != currentPointPosition)
            {
                Gizmos.DrawLine(prevPointPosition + pathGizmosOffset, currentPointPosition + pathGizmosOffset);
            }
            prevPointPosition = currentPointPosition;
        }
    }
}
