﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimations : MonoBehaviour
{
    private Animator _anim;
    private Character _behaviour;

    private Coroutine fightingCoroutine;

    // nazwa stanu -> nazwa boola/triggera w animatorze
    private Dictionary<CharacterState, string> stateToAnimationDictionary =
        new Dictionary<CharacterState, string>()
        {
            { CharacterState.Fighting, "Fighting" },
            { CharacterState.Idle, "Idle" },
            { CharacterState.Walking, "Walking" },
            { CharacterState.Dead, "Dead" },
            { CharacterState.Celebrating, "Celebrating" }
        };

    private void Start()
    {
        _anim = GetComponent<Animator>();
        _behaviour = GetComponent<Character>();

        EventsManager.current.onApplyDamage += GetHitAnimation;
    }

    private void Update()
    {
        FitAnimationToCurrentState();
    }

    private void OnDestroy()
    {
        EventsManager.current.onApplyDamage -= GetHitAnimation;
    }

    private void FitAnimationToCurrentState()
    {
        CharacterState currentState = _behaviour.GetState();
        _anim.SetFloat("Speed", _behaviour.GetStats()[CharacterStatType.MoveSpeed]);

        // jak martwy, to niech umiera
        if (currentState == CharacterState.Dead)
        {
            _anim.SetBool(stateToAnimationDictionary[currentState], true);
        }

        // jeśli ma nic nie robić, to niech nie robi, no trudno
        if (currentState == CharacterState.Idle)
        {
            _anim.SetBool(stateToAnimationDictionary[currentState], true);
        }
        else if (currentState != CharacterState.Fighting)
        {
            _anim.SetBool(stateToAnimationDictionary[CharacterState.Idle], false);
        }

        // jeśli ma patrolować, to niech chodzi
        if (currentState == CharacterState.Walking)
        {
            _anim.SetBool(stateToAnimationDictionary[currentState], true);
        }
        else
        {
            _anim.SetBool(stateToAnimationDictionary[CharacterState.Walking], false);
        }

        // jeśli ma walczyć, to niech będzie idle i odpali cyklicznie animację ataku
        if (currentState == CharacterState.Fighting && fightingCoroutine == null)
        {
            _anim.SetBool(stateToAnimationDictionary[CharacterState.Idle], true);
            fightingCoroutine = StartCoroutine(FightingAnimation());
        }
        else if (currentState != CharacterState.Fighting && fightingCoroutine != null)
        {
            StopCoroutine(fightingCoroutine);
            fightingCoroutine = null;
        }
    }

    private IEnumerator FightingAnimation()
    {
        // uruchamia trigger ataku co ustalony w _behaviour czas
        while (true)
        {
            _anim.SetTrigger(stateToAnimationDictionary[CharacterState.Fighting]);
            yield return new WaitForSeconds(_behaviour.GetStats()[CharacterStatType.AttackInterval]);
        }
    }

    private void GetHitAnimation(int? id, DamageOptions[] damageOptions)
    {
        if (id != gameObject.GetInstanceID()) { return; }
        _anim.SetTrigger("Getting Hit");
    }
}
