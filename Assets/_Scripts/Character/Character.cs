﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Character : MonoBehaviour
{
    #region debug
    public bool debug;
    #endregion

    #region prywatne zmienne

    protected string typeName;

    protected Texture2D portraitTexture;

    protected DamageOptions[] attackOptions;
    protected DamageOptions[] vulnerabilities;
    protected CharacterStats initialStats;

    protected CharacterStats stats;
    protected List<CharacterStatsEffect> statsEffects = new List<CharacterStatsEffect>();
    protected CharacterState state;

    protected int? attackTargetId;

    protected Coroutine currentCoroutine;

    private float rotationSpeed = 720f;

    private float stepSoundTimer;

    private CharacterSounds sounds;
    #endregion

    #region publiczne metody

    public void SetInitials(CharacterType characterType)
    {
        if (typeName != null) { return; }
        typeName = characterType.typeName;
        attackOptions = characterType.attackOptions;
        vulnerabilities = characterType.vulnerabilities;

        CharacterStatsInitValue[] initialValues = new CharacterStatsInitValue[6];
        initialValues[0] = new CharacterStatsInitValue(CharacterStatType.AttackInterval, characterType.attackInterval);
        initialValues[1] = new CharacterStatsInitValue(CharacterStatType.Health, characterType.health);
        initialValues[2] = new CharacterStatsInitValue(CharacterStatType.Luck, characterType.luck);
        initialValues[3] = new CharacterStatsInitValue(CharacterStatType.MoveSpeed, characterType.moveSpeed);
        initialValues[4] = new CharacterStatsInitValue(CharacterStatType.TrapDisablingChance, characterType.trapDisablingChance);
        initialValues[5] = new CharacterStatsInitValue(CharacterStatType.TrapDisablingTime, characterType.trapDisablingTime);

        initialStats = new CharacterStats(initialValues);
    }

    public Dictionary<CharacterStatType, float> GetStats()
    {
        return stats.GetStats();
    }

    public Dictionary<CharacterStatType, float> GetInitialStats()
    {
        return initialStats.GetStats();
    }

    public CharacterState GetState()
    {
        return state;
    }

    public bool IsDead()
    {
        return state == CharacterState.Dead;
    }
    #endregion

    #region prywatne metody
    protected virtual void Start()
    {
        if (debug) { Debug.Log("Character start"); }
        gameObject.name = gameObject.GetInstanceID().ToString();
        stats = new CharacterStats(initialStats);
        AddEventActions();
        if (debug) { Debug.Log("Character start end"); }

        sounds = GetComponent<CharacterSounds>();
    }

    protected virtual void Update()
    {
        DeathCheck();
        if (IsDead()) { return; }
    }

    protected virtual void OnDestroy()
    {
        RemoveEventActions();
    }

    // dodaję reakcje na eventy zachodzące w świecie
    private void AddEventActions()
    {
        EventsManager.current.onApplyDamage += ApplyDamage;
        EventsManager.current.onApplyStatsEffect += ApplyStatsEffect;
        EventsManager.current.onFightWith += FightWith;
        EventsManager.current.onStopFightingWith += StopFightingWith;
    }

    // usuwam reakcje na eventy zachodzące w świecie
    private void RemoveEventActions()
    {
        EventsManager.current.onApplyDamage -= ApplyDamage;
        EventsManager.current.onApplyStatsEffect -= ApplyStatsEffect;
        EventsManager.current.onFightWith -= FightWith;
        EventsManager.current.onStopFightingWith -= StopFightingWith;
    }

    private void ApplyStatsEffect(int id, CharacterStatsEffect effect)
    {
        if (id != gameObject.GetInstanceID()) { return; }

        if (IsDead()) { return; }

        if (debug)
        { Debug.Log("otrzymano stat effects: " + effect.ToString()); }
    }

    private void ApplyDamage(int? id, DamageOptions[] damageOptionsList)
    {
        if (debug) { Debug.Log("ApplyDamage"); }
        if (id != gameObject.GetInstanceID()) { return; }

        if (debug) { Debug.Log("id is ok"); }
        if (IsDead()) { return; }
        if (AvoidDamage()) { return; }
        if (debug) { Debug.Log("not avoiding"); }

        float totalDamage = 0f;
        for (int i = 0; i < damageOptionsList.Length; i++)
        {
            DamageOptions currentDamageOption = damageOptionsList[i];
            float finalDamageTaken = currentDamageOption.power *
                                     Array.Find(vulnerabilities, option => option.type == currentDamageOption.type)
                                         .power;
            totalDamage -= finalDamageTaken;
        }
        if (debug) { Debug.Log("total damage"); }
        if (debug) { Debug.Log(totalDamage); }
        float newStatValue = stats.ModifyStat(CharacterStatType.Health, totalDamage);
        if (debug) { Debug.Log("new stat value"); }
        if (debug) { Debug.Log(newStatValue); }
    }

    private void FightWith(int? id, GameObject enemy)
    {
        if (id != gameObject.GetInstanceID()) { return; }

        if (IsDead()) { return; }

        if (attackTargetId != null) { return; }

        attackTargetId = enemy.GetInstanceID();
        StopAllCoroutines();
        currentCoroutine = StartCoroutine(Fight(enemy));
    }

    private void StopFightingWith(int id)
    {
        if (attackTargetId == id) { attackTargetId = null; }
    }

    private void StopFightingWithMe()
    {
        EventsManager.current.StopFighingtWith(gameObject.GetInstanceID());
    }

    private IEnumerator Fight(GameObject enemy)
    {
        yield return StartCoroutine(RotateTo(enemy.transform.position));
        state = CharacterState.Fighting;
        while (attackTargetId != null)
        {
            GiveDamage((int)attackTargetId);
            EventsManager.current.FightWith(attackTargetId, gameObject); // za każdym ciosem wołamy "Walcz ze mną"
            yield return new WaitForSeconds(GetStats()[CharacterStatType.AttackInterval]);
        }
        state = CharacterState.Idle;
    }

    private void ResetStepSoundTimer()
    {
        stepSoundTimer = 0f;
    }

    private void StepSoundStep()
    {
        stepSoundTimer += Time.deltaTime;
        if (stepSoundTimer > 0.62 / stats.GetStats()[CharacterStatType.MoveSpeed]) // wartość dobrana eksperymentalnie
        {
            List<Sound> footstepSounds = sounds.sounds.Where(sound => sound.name == "footstep").ToList();
            if (footstepSounds.Count == 0) { return; }
            footstepSounds[Random.Range(0, footstepSounds.Count)].source.Play();
            ResetStepSoundTimer();
        }
    }

    protected IEnumerator RotateTo(GameObject rotationObject)
    {
        yield return RotateTo(rotationObject.transform.position);
    }

    protected IEnumerator RotateTo(Vector3 rotationPoint)
    {
        if (debug)
        { Debug.Log("Starting rotation"); }
        Vector3 directionVector = (rotationPoint - transform.position).normalized;
        while (Mathf.Abs(Quaternion.Angle(transform.rotation, Quaternion.LookRotation(directionVector))) > 1f)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(directionVector), rotationSpeed * GetStats()[CharacterStatType.MoveSpeed] * Time.deltaTime);
            yield return null;
        }
        transform.rotation = Quaternion.LookRotation(directionVector);
        if (debug)
        { Debug.Log("Rotated"); }
    }

    protected IEnumerator WalkTo(Vector3 newPosition)
    {
        state = CharacterState.Walking;
        yield return StartCoroutine(RotateTo(newPosition));
        ResetStepSoundTimer();
        while (Vector3.Distance(transform.position, newPosition) > 0.01f)
        {
            StepSoundStep();
            MoveTowards(newPosition);
            yield return null;
        }
        transform.position = newPosition;
        state = CharacterState.Idle;
    }

    private void MoveTowards(Vector3 newPosition)
    {
        transform.position = Vector3.MoveTowards(transform.position, newPosition, GetStats()[CharacterStatType.MoveSpeed] * Time.deltaTime);
    }

    private void GiveDamage(int enemyId)
    {
        EventsManager.current.ApplyDamage(enemyId, attackOptions);
    }

    private bool AvoidDamage()
    {
        return UnityEngine.Random.Range(float.Epsilon, 1f) <= GetStats()[CharacterStatType.Luck];
    }

    private void DeathCheck()
    {
        if (IsDead()) { return; }
        if (GetStats()[CharacterStatType.Health] == 0)
        {
            state = CharacterState.Dead;
            StopAllCoroutines();
            StopFightingWithMe();
            StartCoroutine(DestroyAfterDeath());
        }
    }

    private void ManageStatEffects()
    {
        // jeszcze nie wiem jak się za to zabrać
    }

    protected virtual IEnumerator DestroyAfterDeath()
    {
        Animator _anim;
        if (TryGetComponent<Animator>(out _anim)) {
            yield return new WaitForSeconds(_anim.GetCurrentAnimatorStateInfo(0).length);
        }
        yield return new WaitForSeconds(3f); // TODO: stworzyć jakiś globalny obiekt z opcjami gry i z niego brać czas znikania trupów
        Destroy(gameObject);
    }
    #endregion
}

public enum CharacterState
{
    Walking,
    Fighting,
    Idle,
    Dead,
    Celebrating
}
