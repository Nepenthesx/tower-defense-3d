﻿[System.Serializable]
public class CharacterStatsEffect
{
    public CharacterStatType type;
    public float power;
    public Type effect;
    public enum Type
    {
        Multiplier,
        Additive
    }
    public float duration;
    public bool continuous;

    public CharacterStatsEffect(CharacterStatType statType, Type effectType, float effectPower)
    {
        SetBasicValues(statType, effectType, power);
    }

    public CharacterStatsEffect(CharacterStatType statType, Type effectType, float effectPower, float duration)
    {
        SetBasicValues(statType, effectType, power);
        this.duration = duration;
    }

    public CharacterStatsEffect(CharacterStatType statType, Type effectType, float effectPower, float duration, bool continuous)
    {
        SetBasicValues(statType, effectType, power);
        this.duration = duration;
        this.continuous = continuous;
    }

    private void SetBasicValues(CharacterStatType statType, Type effectType, float effectPower)
    {
        type = statType;
        effect = effectType;
        power = effectPower;
        duration = 0;
        continuous = false;
    }
}
