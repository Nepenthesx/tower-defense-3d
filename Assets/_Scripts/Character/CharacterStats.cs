﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CharacterStats
{
    [SerializeField]
    private Dictionary<CharacterStatType, float> stats;
    private Dictionary<CharacterStatType, float> defaultValues = new Dictionary<CharacterStatType, float>() {
        { CharacterStatType.Health, 1f },
        { CharacterStatType.MoveSpeed, 1f },
        { CharacterStatType.AttackInterval, 1f },
        { CharacterStatType.Luck, 0f },
        { CharacterStatType.TrapDisablingChance, 0f},
        { CharacterStatType.TrapDisablingTime, 0f}
    };
    [SerializeField]
    private Dictionary<CharacterStatType, float> maxValues = new Dictionary<CharacterStatType, float>() {
        { CharacterStatType.Health, Single.MaxValue },
        { CharacterStatType.MoveSpeed, Single.MaxValue },
        { CharacterStatType.AttackInterval, Single.MaxValue },
        { CharacterStatType.Luck, 1f },
        { CharacterStatType.TrapDisablingChance, 1f},
        { CharacterStatType.TrapDisablingTime, Single.MaxValue}
    };
    [SerializeField]
    private Dictionary<CharacterStatType, float> minValues = new Dictionary<CharacterStatType, float>() {
        { CharacterStatType.Health, 0f },
        { CharacterStatType.MoveSpeed, 0f },
        { CharacterStatType.AttackInterval, 0f },
        { CharacterStatType.Luck, 0f },
        { CharacterStatType.TrapDisablingChance, 0f},
        { CharacterStatType.TrapDisablingTime, 0f}
    };

    // wstawia defaultowe statsy
    public CharacterStats()
    {
        stats = defaultValues;
    }

    // wstawia defaultowe statsy
    public CharacterStats(CharacterStats statsToCopy)
    {
        stats = new Dictionary<CharacterStatType, float>();
        foreach (KeyValuePair<CharacterStatType, float> stat in statsToCopy.stats)
        {
            stats[stat.Key] = stat.Value;
        }
    }

    // wstawia podane statsy, a resztę defaultowo
    public CharacterStats(Dictionary<CharacterStatType, float> initialStats)
    {
        stats = new Dictionary<CharacterStatType, float>();
        foreach (KeyValuePair<CharacterStatType, float> kvp in defaultValues)
        {
            stats[kvp.Key] = initialStats.ContainsKey(kvp.Key) ? initialStats[kvp.Key] : defaultValues[kvp.Key];
        }
    }

    // wstawia podane statsy z initialStats
    public CharacterStats(CharacterStatsInitValue[] initialStats)
    {
        stats = new Dictionary<CharacterStatType, float>();

        foreach (CharacterStatsInitValue initValue in initialStats)
        {
            stats[initValue.type] = initValue.value;
        }

        foreach (KeyValuePair<CharacterStatType, float> kvp in defaultValues)
        {
            stats[kvp.Key] = stats.ContainsKey(kvp.Key) ? stats[kvp.Key] : defaultValues[kvp.Key];
        }
    }

    public Dictionary<CharacterStatType, float> GetStats() { return stats; }

    /// <summary>
    /// Increase or decrease the selected stat type by the given value.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public float ModifyStat(CharacterStatType type, float value)
    {
        stats[type] += value;
        ClampStatValues();
        return stats[type];
    }

    /// <summary>
    /// Clamp stats to min/max values.
    /// </summary>
    private void ClampStatValues()
    {
        foreach (CharacterStatType type in Enum.GetValues(typeof(CharacterStatType)))
        {
            if (stats[type] > maxValues[type]) { stats[type] = maxValues[type]; }
            if (stats[type] < minValues[type]) { stats[type] = minValues[type]; }
        }
    }
}

public enum CharacterStatType
{
    Health,
    MoveSpeed,
    AttackInterval,
    Luck,
    TrapDisablingChance,
    TrapDisablingTime
}

[Serializable]
public class CharacterStatsInitValue
{
    public CharacterStatType type;
    public float value;

    public CharacterStatsInitValue(CharacterStatType type, float value)
    {
        this.type = type;
        this.value = value;
    }
}
