﻿using UnityEngine;

public class CharacterSounds : MonoBehaviour
{
    public Sound[] sounds;

    private void Start()
    {
        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;

            sound.source.volume = FindObjectOfType<OptionsManager>().GetComponent<OptionsManager>().soundVolume;
            sound.source.pitch = sound.pitch;

            sound.source.loop = sound.loop;
            sound.source.playOnAwake = false;
        }
    }
}
