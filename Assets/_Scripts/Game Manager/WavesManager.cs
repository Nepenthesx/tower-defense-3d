﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Pozwala na zarządzanie falami przeciwników.
/// </summary>
public class WavesManager : MonoBehaviour
{
    public WaveConfig[] waves;
    public int currentWave = 0;

    //Obiekty na NextWavePanel
    [SerializeField] private Button nextWaveButton;
    [SerializeField] private Text panelText;
    [SerializeField] private Text countdownText;
    [SerializeField] private Text waveCountText;
    [SerializeField] private Text profitText;
    [SerializeField] private Image[] wavePartInfoPlace = new Image[8];

    private float endShowProfitTime;
    private Vector3 profitTextPos;

    public WaveConfig GetNextWave()
    {
        if (currentWave >= waves.Length) { return null;  }
        return waves[currentWave];
    }

    private void Start()
    {
        FillNextWavePanel(currentWave);
        waveCountText.text = "";
        profitText.text = "";
        profitTextPos = profitText.transform.position;
    }

    private void Update()
    {
        if (currentWave < waves.Length && waves[currentWave] == null)
        {
            Debug.Log("Nie ustawiłeś żadnej fali w WavesManager");
        }
        if (currentWave < waves.Length && waves[currentWave].startTime <= Time.timeSinceLevelLoad)
        {
            StartCoroutine(SpawnWave(waves[currentWave]));
            currentWave++;

            waveCountText.text = currentWave.ToString();

            if (currentWave < waves.Length)
                FillNextWavePanel(currentWave);
            else
                ClearNextWavePanel();

        }

        //Odlicza czas do następnej fali, zmienia tekst na panelu
        if (currentWave < waves.Length)
        {
            countdownText.text = (Mathf.CeilToInt(waves[currentWave].startTime - Time.timeSinceLevelLoad)).ToString();

            //Jeśli to jest/nie jest pierwsza fala
            if (currentWave == 0)
                panelText.text = "First Wave in...";
            else
                panelText.text = "Next Wave in...";
        }
        //Jeśli to ostatnia fala - niszczy przycisk następnej fali, zmienia tekst na panelu
        else
        {
            if (nextWaveButton)
                Destroy(nextWaveButton.gameObject);
            panelText.text = "Last Wave!";
            countdownText.text = "";
        }
        
        //Jeśli do następnej fali jest więcej niż 20 sekund
        if (currentWave < waves.Length && Mathf.CeilToInt(waves[currentWave].startTime - Time.timeSinceLevelLoad) > 20.0f)
        {
            //Zamienia przycisk następnej fali na nieaktywny i wypełnia z każdą klatką do 20 sekund (wtedy będzie cały wypełniony)
            nextWaveButton.interactable = false;
            //nextWaveButton.image.fillAmount = (waves[currentWave].startTime - waves[currentWave - 1].startTime - 20) / 20;
            nextWaveButton.image.fillAmount = (Time.timeSinceLevelLoad - waves[currentWave - 1].startTime) / (waves[currentWave].startTime - waves[currentWave - 1].startTime - 20);
        }
        else if (currentWave < waves.Length)
        {
            //Zamienia przycisk następnej fali na aktywny i całkiwicie wypełnia przycisk
            nextWaveButton.interactable = true;
            nextWaveButton.image.fillAmount = 1;
        }

        //Jeśli profitText nie jest pusty, wyświetla informacje o zysku przez 1.5 sekundy
        if (profitText.text != "")
        {
            profitText.rectTransform.position += Vector3.up * 1.2f;

            if (Time.time > endShowProfitTime)
            {
                profitText.text = "";
                profitText.transform.position = profitTextPos;
            }
        }
    }

    /// <summary>
    /// Spawnuje przeciwnika w wyznaczonym miejscu, idącego po określonej ścieżce
    /// </summary>
    /// <param name="enemy"></param>
    /// <param name="spawner"></param>
    /// <param name="path"></param>
    private void SpawnEnemy(CharacterType enemy, Transform spawner, PathScript path)
    {
        GameObject newEnemy = GameObject.Instantiate(enemy.prefab, spawner.position, Quaternion.LookRotation(path.pointList[0].transform.position - spawner.position));
        Enemy enemyComponent = newEnemy.AddComponent<Enemy>();
        enemyComponent.InitializePath(path);
        enemyComponent.SetInitials(enemy);
    }

    /// <summary>
    /// Rozpoczyna proces spawnowania fali przeciwników
    /// </summary>
    /// <param name="wave"></param>
    /// <returns></returns>
    private IEnumerator SpawnWave(WaveConfig wave)
    {
        int wavePartNumber = 0;
        while (wavePartNumber < wave.waveParts.Length)
        {
            // TODO: Prawdopodobnie trzeba będzie zmienić ten system tak, żeby pozwalał na wyruszanie kilku grup przeciwników na raz, jeżeli nie ruszają z tego samego miejsca
            // ...ergo: wydzielić kolejną korutynę i kontrolować czy w danym spawn poincie coś już wychodzi
            WavePart wavePart = wave.waveParts[wavePartNumber];
            int enemyNumber = 0;
            while (enemyNumber < wavePart.enemiesNumber)
            {
                SpawnEnemy(wavePart.enemyType, wavePart.spawner, wavePart.path);
                enemyNumber++;
                yield return new WaitForSeconds(wavePart.spawningInterval);
            }
            wavePartNumber++;
        }
    }

    /// <summary>
    /// Puszcza następną falę przeciwników i przynosi zysk w zależności od czasu
    /// </summary>
    public void NextWave()
    {
        //Różnica czasu między obecnym czasem (Time.time) a czasem puszczenia najbliższej fali (startTime)
        float deltaTime;
        deltaTime = waves[currentWave].startTime - Time.timeSinceLevelLoad;

        //Zwiększa pieniądze (tyle ile sekund zostało do końca) i uzupełnia o tym tekst
        EventsManager.current.SkipInterwaveTime(Mathf.CeilToInt(deltaTime));
        profitText.text = " +" + Mathf.CeilToInt(deltaTime).ToString() + " $";
        endShowProfitTime = Time.timeSinceLevelLoad + 1.5f;

        //Skraca czas puszczenia każdej fali o deltaTime
        for (int i = currentWave; i < waves.Length; i++)
        {
            waves[i].startTime -= deltaTime;
        }
    }

    /// <summary>
    /// Uzupełnia informacje o przeciwnikach w następnej fali
    /// </summary>
    private void FillNextWavePanel(int nextWaveIndex)
    {
        //Zapełnia miejsca w panelu (ikonka i ilość przeciwników) od 1 do 8 (jeśli jest mniej części fali niż 8 to reszte zostawia puste)
        for (int i = 0; i < 8; i++)
        {
            if (i < waves[nextWaveIndex].waveParts.Length)
            {
                wavePartInfoPlace[i].sprite = waves[nextWaveIndex].waveParts[i].enemyType.icon;
                wavePartInfoPlace[i].color = new Color(1, 1, 1, 1);
                wavePartInfoPlace[i].GetComponentInChildren<Text>().text = "x" + waves[nextWaveIndex].waveParts[i].enemiesNumber.ToString();
            }
            else
            {
                wavePartInfoPlace[i].sprite = null;
                wavePartInfoPlace[i].color = new Color(0, 0, 0, 0);
                wavePartInfoPlace[i].GetComponentInChildren<Text>().text = "";
            }
        }
    }

    /// <summary>
    /// Usuwa wszystkie informacje o przeciwnikach w następnej fali
    /// </summary>

    private void ClearNextWavePanel()
    {
        for (int i = 0; i < 8; i++)
        {
            wavePartInfoPlace[i].sprite = null;
            wavePartInfoPlace[i].color = new Color(0, 0, 0, 0);
            wavePartInfoPlace[i].GetComponentInChildren<Text>().text = "";
        }
    }
}
