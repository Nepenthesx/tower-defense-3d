﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyManager : MonoBehaviour
{
    [SerializeField]
    private int money;
    
    [SerializeField]
    private Text moneyAmountText;

    private void Start()
    {
        EventsManager.current.onEnemyDead += AddEnemyValue;
        EventsManager.current.onBuildTrap += Subtract;
        EventsManager.current.onSellTrap += Add;
        EventsManager.current.onSkipInterwaveTime += Add;
    }

    private void Update()
    {
        moneyAmountText.text = GetMoney().ToString();
    }

    private void OnDestroy()
    {
        EventsManager.current.onEnemyDead -= AddEnemyValue;
        EventsManager.current.onBuildTrap -= Subtract;
        EventsManager.current.onSellTrap -= Add;
        EventsManager.current.onSkipInterwaveTime -= Add;
    }

    public int GetMoney()
    { return money; }

    private void AddEnemyValue(Enemy enemy)
    {
        Add(enemy.worthValue);
    }

    private void Add(int value)
    {
        money += value;
    }

    private void Subtract(int value)
    {
        money -= value;
        if (money < 0)
        {
            money = 0;
        }
    }
}
