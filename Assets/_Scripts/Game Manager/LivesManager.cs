﻿using UnityEngine;
using UnityEngine.UI;

public class LivesManager : MonoBehaviour
{

    private bool gameOver = false;
    public bool GetGameOver()
    { return gameOver; }

    [SerializeField]
    private int livesLeft;
    public int GetLivesLeft()
    { return livesLeft; }

    [SerializeField]
    private Text livesAmountText;

    private void Start()
    {
        EventsManager.current.onReducePlayerLive += ReduceLife;
    }

    private void Update()
    {
        livesAmountText.text = GetLivesLeft().ToString();
    }

    private void OnDestroy()
    {
        EventsManager.current.onReducePlayerLive -= ReduceLife;
    }

    public void ReduceLife(int value)
    {
        livesLeft -= value;
        if (livesLeft <= 0)
        {
            livesLeft = 0;
            EventsManager.current.GameOver();
        }
    }
}
