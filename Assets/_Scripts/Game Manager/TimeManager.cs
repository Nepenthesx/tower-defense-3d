﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    [SerializeField] private bool paused;
    [SerializeField] private Button pauseButton;

    [SerializeField] private float speededUpTimeScale;
    [SerializeField] private Button changeTimeSpeedButton;
    [SerializeField] private Sprite speedUpSprite;
    [SerializeField] private Sprite speedUpHighlightedSprite;
    [SerializeField] private Sprite slowDownSprite;
    [SerializeField] private Sprite slowDownHighlightedSprite;

    [SerializeField] [Range(0f, 5f)] private float timeScale = 1f;

    private float previousTimeScale;

    private void Start()
    {
        EventsManager.current.onGameOver += PauseGame;

        pauseButton.onClick.AddListener(PauseGame);
        changeTimeSpeedButton.onClick.AddListener(SpeedUpTime);
    }

    private void OnDestroy()
    {
        EventsManager.current.onGameOver -= PauseGame;
    }

    public void PauseGame()
    {
        pauseButton.onClick.RemoveAllListeners();
        pauseButton.onClick.AddListener(UnpauseGame);

        paused = true;
        timeScale = 0;
        Time.timeScale = 0;
    }

    public void UnpauseGame()
    {
        pauseButton.onClick.RemoveAllListeners();
        pauseButton.onClick.AddListener(PauseGame);

        paused = false;
        timeScale = previousTimeScale;
    }

    public void SpeedUpTime()
    {
        timeScale = speededUpTimeScale;

        //Zmienia wygląd przycisku
        SpriteState spriteState = new SpriteState
        {
            highlightedSprite = slowDownHighlightedSprite
        };
        changeTimeSpeedButton.GetComponent<Image>().sprite = slowDownSprite;
        changeTimeSpeedButton.GetComponent<Button>().spriteState = spriteState;

        //Dodaje funkcję OnClick jako SlowDownTime
        changeTimeSpeedButton.GetComponent<Button>().onClick.RemoveAllListeners();
        changeTimeSpeedButton.GetComponent<Button>().onClick.AddListener(SlowDownTime);
        return;
    }

    public void SlowDownTime()
    {
        timeScale = 1;

        //Zmienia wygląd przycisku
        SpriteState spriteState = new SpriteState
        {
            highlightedSprite = speedUpHighlightedSprite
        };
        changeTimeSpeedButton.GetComponent<Image>().sprite = speedUpSprite;
        changeTimeSpeedButton.GetComponent<Button>().spriteState = spriteState;

        //Dodaje funkcję OnClick jako SlowDownTime
        changeTimeSpeedButton.GetComponent<Button>().onClick.RemoveAllListeners();
        changeTimeSpeedButton.GetComponent<Button>().onClick.AddListener(SpeedUpTime);
        return;
    }

    private void Update()
    {
        if (paused || previousTimeScale == timeScale) { return; }
        Time.timeScale = timeScale;
        previousTimeScale = Time.timeScale;
    }
}
