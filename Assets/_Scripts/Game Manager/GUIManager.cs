﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIManager : MonoBehaviour
{
    private MoneyManager moneyManager;
    private WavesManager wavesManager;
    private LivesManager livesManager;

    private Vector2 resolution;

    private bool gameOver;

    void Start()
    {
        EventsManager.current.onGameOver += ShowGameOverScreen;

        moneyManager = GetComponent<MoneyManager>(); // TODO: docelowo bazować tylko na eventach a nie na zależnościach
        wavesManager = GetComponent<WavesManager>();
        livesManager = GetComponent<LivesManager>();

        resolution = new Vector2(Camera.main.scaledPixelWidth, Camera.main.scaledPixelHeight);
    }
    
    private void OnDestroy()
    {
        EventsManager.current.onGameOver -= ShowGameOverScreen;
    }

    /*
    private void OnGUI()
    {
        if (gameOver)
        {
            GUI.Box(new Rect(new Vector2(150f, 150f), new Vector2(resolution.x * 0.5f, resolution.y * 0.5f)), GenerateGameOverText());
        }
        else
        {
            GUI.Box(new Rect(new Vector2(20f, 20f), new Vector2(resolution.x * 0.15f, resolution.y * 0.15f)), GenerateText());
        }
    }
    */

    private void ShowGameOverScreen()
    {
        gameOver = true;
    }

    private string GenerateText()
    {
        WaveConfig nextWave = wavesManager.GetNextWave();

        string livesText = "Lives left: " + livesManager.GetLivesLeft();
        string moneyText = "Money: " + moneyManager.GetMoney();
        string nextWaveText = nextWave != null ? (nextWave.startTime - Time.time).ToString("#.#") + " to next wave containing: \n" + wavesManager.GetNextWave().GenerateString() : "";

        return livesText + "\n" + moneyText + "\n" + nextWaveText;
        
    }

    private string GenerateGameOverText()
    {
        return "GAME OVER, LOSER!"; // TODO: jakieś podsumowanie, statystyki, czy coś
    }
}
