﻿using System;
using UnityEngine;

public class EventsManager: MonoBehaviour
{
    public static EventsManager current;

    private void Awake()
    {
        current = this;
    }

    public event Action<int?, DamageOptions[]> onApplyDamage;
    public void ApplyDamage(int? id, DamageOptions[] damageOptions)
    {
        onApplyDamage?.Invoke(id, damageOptions);
    }

    public event Action<int, CharacterStatsEffect> onApplyStatsEffect;
    public void ApplyStatsEffect(int id, CharacterStatsEffect effect)
    {
        onApplyStatsEffect?.Invoke(id, effect);
    }

    public event Action<int?, GameObject> onFightWith;
    public void FightWith(int? id, GameObject enemy)
    {
        onFightWith?.Invoke(id, enemy);
    }

    public event Action<int> onStopFightingWith;
    public void StopFighingtWith(int id)
    {
        onStopFightingWith?.Invoke(id);
    }

    public event Action<int> onReducePlayerLive;
    public void ReducePlayerLive(int value)
    {
        onReducePlayerLive?.Invoke(value);
    }

    public event Action onGameOver;
    public void GameOver()
    {
        onGameOver?.Invoke();
    }

    public event Action onGameStart;
    public void GameStart()
    {
        onGameStart?.Invoke();
    }

    public event Action<Enemy> onEnemyDead;
    public void EnemyDead(Enemy enemy)
    {
        onEnemyDead?.Invoke(enemy);
    }

    public event Action<int> onBuildTrap; // TODO: przerobić pewnie tak, żeby przekazywać info o całej pułapce/ulepszeniu
    public void BuildTrap(int money)
    {
        onBuildTrap?.Invoke(money);
    }

    public event Action<int> onSellTrap; // TODO: przerobić pewnie tak, żeby przekazywać info o całej pułapce/ulepszeniu
    public void SellTrap(int money)
    {
        onSellTrap?.Invoke(money);
    }

    //Time = money
    public event Action<int> onSkipInterwaveTime;
    public void SkipInterwaveTime(int time)
    {
        onSkipInterwaveTime?.Invoke(time);
    }

    public void X(Vector3 a)
    {

    }
}
