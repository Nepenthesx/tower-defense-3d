﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    [SerializeField] private GameObject optionsPanelPref;
    [SerializeField] [Range(0f, 1f)] public float soundVolume = 0.8f;
    private GameObject[] soundSquares = new GameObject[5];

    [SerializeField] [Range(0f, 1f)] public float musicVolume = 0.8f;
    private GameObject[] musicSquares = new GameObject[5];

    [SerializeField] private Sprite squareEmptySprite;
    [SerializeField] private Sprite squareFullSprite;

    private bool soundVolumeChange = true;
    private bool musicVolumeChange = true;

    private GameObject optionsPanel;

    private void Update()
    {
        //Jeśli nastąpiła zmiana głośności dźwięku
        if (soundVolumeChange)
        {
            foreach (AudioSource sound in GetAllSoundsByTag("Enemy"))
            {
                sound.volume = soundVolume;
            }
            foreach (AudioSource sound in GetAllSoundsByTag("Trap"))
            {
                sound.volume = soundVolume;
            }

            soundVolumeChange = false;
        }

        if (musicVolumeChange)
        {
            musicVolumeChange = false;
        }
    }

    /// <summary>
    /// Tworzy okienko z opcjami gry
    /// </summary>
    public void CreateOptionsPanel()
    {
        if (optionsPanel)
            return;

        optionsPanel = Instantiate(optionsPanelPref, FindObjectOfType<Canvas>().transform) as GameObject;

        //Przypisuje odpowiednie dane do OptionsManager
        foreach (Transform child in optionsPanel.transform)
        {
            switch (child.name)
            {
                case "SoundSquare0":
                    soundSquares[0] = child.gameObject;
                    break;
                case "SoundSquare1":
                    soundSquares[1] = child.gameObject;
                    break;
                case "SoundSquare2":
                    soundSquares[2] = child.gameObject;
                    break;
                case "SoundSquare3":
                    soundSquares[3] = child.gameObject;
                    break;
                case "SoundSquare4":
                    soundSquares[4] = child.gameObject;
                    break;
                case "SoundArrowLeft":
                    child.GetComponent<Button>().onClick.AddListener(TurnDownSoundVolume);
                    break;
                case "SoundArrowRight":
                    child.GetComponent<Button>().onClick.AddListener(TurnUpSoundVolume);
                    break;
                case "MusicSquare0":
                    musicSquares[0] = child.gameObject;
                    break;
                case "MusicSquare1":
                    musicSquares[1] = child.gameObject;
                    break;
                case "MusicSquare2":
                    musicSquares[2] = child.gameObject;
                    break;
                case "MusicSquare3":
                    musicSquares[3] = child.gameObject;
                    break;
                case "MusicSquare4":
                    musicSquares[4] = child.gameObject;
                    break;
                case "ResumeButton":
                    child.GetComponent<Button>().onClick.AddListener(Resume);
                    break;
                case "MusicArrowLeft":
                    child.GetComponent<Button>().onClick.AddListener(TurnDownMusicVolume);
                    break;
                case "MusicArrowRight":
                    child.GetComponent<Button>().onClick.AddListener(TurnUpMusicVolume);
                    break;
            }
        }

        int soundSquaresCount = (int)(soundVolume * 5);
        for (int i = 0; i < 5; i++)
        {
            if (i < soundSquaresCount)
                soundSquares[i].GetComponent<Image>().sprite = squareFullSprite;
            else
                soundSquares[i].GetComponent<Image>().sprite = squareEmptySprite;
        }

        int musicSquaresCount = (int)(musicVolume * 5);
        for (int i = 0; i < 5; i++)
        {
            if (i < musicSquaresCount)
                musicSquares[i].GetComponent<Image>().sprite = squareFullSprite;
            else
                musicSquares[i].GetComponent<Image>().sprite = squareEmptySprite;
        }
    }

    /// <summary>
    /// Wyszukuje wszystkie komponenty AudioSource z grupy objektów o konretnym tagu
    /// </summary>
    private AudioSource[] GetAllSoundsByTag(string tag)
    {
        List<AudioSource> sounds = new List<AudioSource>();
        GameObject[] tagObjects;

        tagObjects = GameObject.FindGameObjectsWithTag(tag);

        foreach (GameObject tagObject in tagObjects)
        {
            foreach (var audioComponent in tagObject.GetComponents<AudioSource>())
            {
                sounds.Add(audioComponent);
            }
        }

        return sounds.ToArray();
    }

    public void TurnUpSoundVolume()
    {
        if (soundVolume <= 0.8f)
            soundVolume += 0.2f;
        else
            return;

        int squaresCount = (int)(soundVolume * 5);
        for (int i = 0; i < 5; i++)
        {
            if (i < squaresCount)
                soundSquares[i].GetComponent<Image>().sprite = squareFullSprite;
            else
                soundSquares[i].GetComponent<Image>().sprite = squareEmptySprite;
        }

        soundVolumeChange = true;
    }

    public void TurnDownSoundVolume()
    {
        if (soundVolume >= 0.2f)
            soundVolume -= 0.2f;
        else
            return;

        int squaresCount = (int)(soundVolume * 5);
        for (int i = 0; i < 5; i++)
        {
            if (i < squaresCount)
                soundSquares[i].GetComponent<Image>().sprite = squareFullSprite;
            else
                soundSquares[i].GetComponent<Image>().sprite = squareEmptySprite;
        }

        soundVolumeChange = true;
    }

    public void TurnUpMusicVolume()
    {
        if (musicVolume <= 0.8f)
            musicVolume += 0.2f;
        else
            return;

        int squaresCount = (int)(musicVolume * 5);
        for (int i = 0; i < 5; i++)
        {
            if (i < squaresCount)
                musicSquares[i].GetComponent<Image>().sprite = squareFullSprite;
            else
                musicSquares[i].GetComponent<Image>().sprite = squareEmptySprite;
        }

        musicVolumeChange = true;
    }

    public void TurnDownMusicVolume()
    {
        if (musicVolume >= 0.2f)
            musicVolume -= 0.2f;
        else
            return;

        int squaresCount = (int)(musicVolume * 5);
        for (int i = 0; i < 5; i++)
        {
            if (i < squaresCount)
                musicSquares[i].GetComponent<Image>().sprite = squareFullSprite;
            else
                musicSquares[i].GetComponent<Image>().sprite = squareEmptySprite;
        }

        musicVolumeChange = true;
    }

    public void Resume()
    {
        Destroy(optionsPanel.gameObject);
    }
}
