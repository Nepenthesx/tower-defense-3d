﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    public Vector3 positionShift;
    public float healthBarMaxWidth = 1;
    public Gradient gradient;
    private GameObject target;
    private Character character;

    private void Start()
    {
        target = transform.parent.gameObject;
        character = target.GetComponent<Character>();
    }

    private void Update()
    {
        float healthFraction = character.GetStats()[CharacterStatType.Health] / character.GetInitialStats()[CharacterStatType.Health];
        if (healthFraction == 0f)
        {
            Destroy(gameObject);
            return;
        }
        transform.position = target.transform.position + positionShift;
        transform.rotation = Quaternion.LookRotation(transform.position - Camera.main.transform.position, Vector3.up);
        transform.localScale = new Vector3(healthBarMaxWidth * healthFraction, transform.localScale.y, transform.localScale.z);
        gameObject.GetComponent<Renderer>().material.color = gradient.Evaluate(1 - healthFraction);
    }
}
