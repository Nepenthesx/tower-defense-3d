﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ally : Character
{
    #region publiczne zmienne
    #endregion
    #region prywatne zmienne
    [SerializeField]
    private Vector3 stationingPoint;
    private float patrollingRange = 0.5f;
    private float waitingTime = 1f;
    #endregion

    #region publiczne metody
    public void SetStationingPoint(Vector3 point)
    {
        stationingPoint = point;
    }
    #endregion

    #region prywatne metody
    protected override void Start()
    {
        base.Start();
        currentCoroutine = StartCoroutine(Patrol());
    }

    protected override void Update()
    {
        base.Update();
        if (currentCoroutine == null)
        {
            currentCoroutine = StartCoroutine(Patrol());
        }
    }

    private IEnumerator Patrol()
    {
        while (true)
        {
            Vector3 newPatrolPosition = stationingPoint + new Vector3(Random.Range(-patrollingRange, patrollingRange), 0, Random.Range(-patrollingRange, patrollingRange));
            currentCoroutine = StartCoroutine(WalkTo(newPatrolPosition));
            yield return currentCoroutine;
            yield return new WaitForSeconds(waitingTime);
        }
    }
    #endregion
}
