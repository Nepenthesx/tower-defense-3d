﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Statystyki jakie posiada pułapka (wszystkie informacje o niej zebrane w jednym miejscu)
/// </summary>
public class TrapStatistics : MonoBehaviour
{
    //Fizyczna, magiczna, wspierająca, przyzywająca (fizyczne i magiczne stworzenie)
    public enum TrapType
    {
        Melee,
        Magic,
        Support,
        SummonMelee,
        SummonMagic
    };


    //Rodzaj
    public TrapType type;
    //Nazwa
    public new string name;
    //Ikonka (w opisie)
    public Sprite icon;
    //Cena zakupu
    public int buildPrice;
    //Cena sprzedaży
    public int sellPrice;

    [Space(10)]

    //Obrażenia: Melee, Magic, Support, Summon
    public float damage; 
    //Czas działania: Melee, Magic, (Support)
    public float actionTime;
    //Czas odnawiania: Melee, Magic, (Support), Summon
    public float renewalTime;
    //Wywoływany efekt: Support
    public string effect;
    //Życie: Summon
    public float health;
    //Szybkość: Summon
    public float speed;
    //Ilość: Summon
    public int count;

    [Space(10)]

    //Możliwość ulepszenia
    public bool canUpgrade;
    //Wersja ulepszona
    public GameObject upgradedTrap;
    //Możliwość sprzedania
    public bool canSell;
}
