﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BuildScript : MonoBehaviour
{
    //Skrypt dołączany do miejsca budowy (BuildingPlace)

    //Lista przycisków (publiczna klasa)
    //WAŻNE Przycisk anulowania powinien mieć indeks 0
    //Przycisk zakupu został usunięty, pułapka kupuje się po naciśnięciu przycisku danej pułapki, opis pojawia się po najechaniu kursora
    public ButtonI[] buttons;

    //Lista przycisków jako obiekty
    private List<GameObject> buttonObjectList = new List<GameObject>();

    //Z której strony jest ściana w danym Tile względem kamery (widza) | 0 - lewo, 1 - tył, 2 - prawo
    public int wallDirection;

    //Przycisk budowy | Domyślnie jest childem "miejsca budowy"
    public GameObject buildButtonPrefab;
    private GameObject buildButton;

    public GameObject framePrefab;
    public GameObject descriptionPrefab;
    public Vector3 descriptionPos;

    private GameObject description;

    //Odległość przycisków od wspólnego środka
    public float radius;

    private Canvas canvas;

    private MoneyManager moneyManager;

    private Vector3 defaultPosition;
    private Quaternion defaultRotation;

    //Opóźnienie po kliknięciu i zbudowaniu menu (By nie wcisnąć od razu anuluj)
    private float clickDelay;

    //Dwie zmienne dotyczące istnienia menu
    //menuExist - zmianna globalna, czy na mapie zostało już stworzone jakieś menu budowy (może być stworzone tylko jedno)
    //myMenuExist - zmienna prywatne, czy menu jest stworzone przez mój obiekt
    public static bool menuExist = false;
    private bool myMenuExist = false;

    private void Awake()
    {
        defaultPosition = transform.position;
        defaultRotation = transform.rotation;
    }

    void Start()
    {
        canvas = FindObjectOfType<Canvas>();
        moneyManager = FindObjectOfType<MoneyManager>();

        //Tworzy przycisk budowy i robi z niego childa mojego obiektu
        buildButton = Instantiate(buildButtonPrefab, transform.position, Quaternion.identity, transform) as GameObject;
    }

    void Update()
    {
        //Jeśli przycisk budowy został naciśnięty i globalne menu nie istnieje
        if (!menuExist && OnClick(buildButton.transform))
        {
            BuildMenu();

            gameObject.AddComponent<RotateToCamera>();

            menuExist = true;
            myMenuExist = true;
        }

        //Jeśli globalne menu istnieje i należy do mojego obiektu
        if (myMenuExist)
        {
            for (int i = 0; i < buttonObjectList.Count; i++)
            {
                //Jeśli na i przycisk zostanie najechany kursor
                if (OnMouse(buttonObjectList[i].transform))
                {
                    //Zmiana wyglądu - podświetlone
                    buttonObjectList[i].GetComponent<SpriteRenderer>().sprite = buttons[i].highlightSprite;
                    //Zmiana opisu
                    ChangeDescription(buttons[i]);
                }
                //Jeśli na i przycisk nie zostanie najechany kursor
                if (!OnMouse(buttonObjectList[i].transform))
                {
                    //Zmiana wyglądu - normalne
                    buttonObjectList[i].GetComponent<SpriteRenderer>().sprite = buttons[i].normalSprite;
                }
                //Jesli na żaden z wszystkich przycisków nie zostanie najechany kursor
                if (NoMouse(buttonObjectList))
                {
                    //Usunięcie opisu
                    ClearDescription();
                }
            }

            for (int i = 0; i < buttonObjectList.Count; i++)
            {
                //Jeśli któryś z przycisków zostanie naciśnięty i zgadza się liczba pieniędzy | Dotyczy wszystkich prócz przycisku anulowania
                if (OnClick(buttonObjectList[i].transform) && i != 0 && moneyManager.GetMoney() >= buttons[i].price)
                {
                    Destroy(gameObject.GetComponent<RotateToCamera>());

                    gameObject.transform.position = defaultPosition;
                    gameObject.transform.rotation = defaultRotation;

                    //Tworzy pułapkę
                    GameObject trap = Instantiate(buttons[i].trapPrefab, defaultPosition + buttons[i].trapPosition, RotatationByWallDirection()) as GameObject;
                    trap.GetComponent<ExtendScript>().defaultPosition = defaultPosition;
                    trap.GetComponent<ExtendScript>().defaultRotation = defaultRotation;
                    trap.GetComponent<ExtendScript>().wallDirection = wallDirection;
                    trap.GetComponent<ExtendScript>().radius = radius;

                    //Odjęcie pieniędzy za budowę
                    EventsManager.current.BuildTrap(buttons[i].price);

                    menuExist = false;
                    myMenuExist = false;

                    //Usunięcie przycisków i samgo obiektu
                    buttonObjectList.Clear();
                    Destroy(gameObject);

                    break;
                }

                //Jeśli przycisk anulowania budowy zostanie naciśnięty
                if (OnClick(buttonObjectList[i].transform) && i == 0 && Time.time > clickDelay)
                {
                    Destroy(gameObject.GetComponent<RotateToCamera>());

                    gameObject.transform.position = defaultPosition;
                    gameObject.transform.rotation = defaultRotation;

                    //Niszczy wszystkie przyciski i buduje z powrotem przycisk budowy
                    buttonObjectList.Clear();
                    foreach (Transform child in transform)
                    {
                        Destroy(child.gameObject);
                    }

                    buildButton = Instantiate(buildButtonPrefab, transform.position, Quaternion.identity, transform) as GameObject;

                    menuExist = false;
                    myMenuExist = false;

                    break;
                }
            }


        }

    }

    private bool OnClick(Transform hitTransform)
    {
        //Ray - promień poprowadzony tam gdzie pozycja kursora
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Jeśli gracz nacisnął LPM
        if (Input.GetMouseButtonDown(0))
        {
            //Jeśli został poprowadzony promień | out hit to informacja zwrotna o zderzeniu promienia z obiektem
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                //Jeśli obiekt, z którym zderzył się promień, jest danym obiektem
                if (hit.transform == hitTransform)
                {
                    return true;
                }
            }
        }

        return false;
    }

    private bool OnMouse(Transform hitTransform)
    {
        //Ray - promień poprowadzony tam gdzie pozycja kursora
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Jeśli został poprowadzony promień | out hit to informacja zwrotna o zderzeniu promienia z obiektem
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            //Jeśli obiekt, z którym zderzył się promień, jest danym obiektem
            if (hit.transform == hitTransform)
            {
                return true;
            }
        }

        return false;
    }

    private bool NoMouse(List<GameObject> hitTransformList)
    {
        //Prowadzi promień przez listę obiektów, jeśli żaden z nich się nie zderzy, zwraca prawdę
        for (int i = 0; i < hitTransformList.Count; i++)
        {
            //Ray - promień poprowadzony tam gdzie pozycja kursora
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            //Jeśli został poprowadzony promień | out hit to informacja zwrotna o zderzeniu promienia z obiektem
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                //Jeśli obiekt, z którym zderzył się promień, jest danym obiektem z listy
                if (hit.transform == hitTransformList[i].transform)
                {
                    return false;
                }
            }
        }

        return true;
    }

    private void BuildMenu()
    {
        //Tworzy obramowanie i opis
        Instantiate(framePrefab, transform.position, Quaternion.identity, canvas.transform);
        description = Instantiate(descriptionPrefab, transform.position + descriptionPos * radius, Quaternion.identity, canvas.transform) as GameObject;

        for (int i = 0; i < buttons.Length; i++)
        {
            Vector3 buttonPos = transform.position + buttons[i].buttonPosition * radius;

            //Tworzy przyciski
            buttonObjectList.Add(Instantiate(buttons[i].buttonPrefab, buttonPos, Quaternion.identity, canvas.transform) as GameObject);
        }

        Destroy(buildButton);

        clickDelay = Time.time + 0.1f;
    }

    private void ChangeDescription(ButtonI button)
    {
        //Podmienia odpwiednio napisy i ikony w opisie pułapki do budowania
        foreach (Transform child in description.transform)
        {
            switch (child.name)
            {
                case "NameText":
                    if (button.icon != null)
                        child.GetComponent<TextMeshPro>().text = button.name;
                    break;
                case "PriceText":
                    if (button.price != 0)
                        child.GetComponent<TextMeshPro>().text = button.price.ToString();
                    break;
                case "DamageText":
                    child.GetComponent<TextMeshPro>().text = button.GetTrapDamage();
                    break;
                case "TimeText":
                    child.GetComponent<TextMeshPro>().text = button.GetTrapTime();
                    break;
                case "SpeedText":
                    child.GetComponent<TextMeshPro>().text = button.GetTrapSpeed();
                    break;
                case "Icon":
                    if (button.icon != null)
                        child.GetComponent<SpriteRenderer>().sprite = button.icon;
                    break;
            }
        }
    }

    public void ClearDescription()
    {
        //Usuwa wszystkie napisy i ikony w opisie pułapki do budowania
        foreach (Transform child in description.transform)
        {
            switch (child.name)
            {
                case "NameText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "PriceText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "DamageText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "TimeText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "SpeedText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "Icon":
                    child.GetComponent<SpriteRenderer>().sprite = null;
                    break;
            }
        }

    }

    private Quaternion RotatationByWallDirection()
    {
        switch (wallDirection)
        {
            //lewo
            case 0:
                return Quaternion.Euler(defaultRotation.x, defaultRotation.y - 90, defaultRotation.z);
            //tył
            case 1:
                return Quaternion.Euler(defaultRotation.x, defaultRotation.y + 0, defaultRotation.z);
            //prawo
            case 2:
                return Quaternion.Euler(defaultRotation.x, defaultRotation.y + 90, defaultRotation.z);
            default:
                return defaultRotation;
        }
    }
    
}

[System.Serializable]
public class ButtonI
{
    public GameObject buttonPrefab;
    public Vector3 buttonPosition;
    public Sprite normalSprite;
    public Sprite highlightSprite;
    public GameObject trapPrefab;
    public Vector3 trapPosition;
    public string name;
    public int price;
    public Sprite icon;

    public string GetTrapDamage()
    {
        if (trapPrefab && trapPrefab.GetComponent<IntervalTrapBehaviour>())
        {
            DamageOptions[] damageOptions = trapPrefab.GetComponent<IntervalTrapBehaviour>().damageOptions;

            //Jak na razie bierze tylko pierwszą wartość obrażeń
            if (damageOptions.Length != 0 && damageOptions[0].power > 0)
            {
                return damageOptions[0].power.ToString();
            }
            else
            {
                return "-";
            }       
        }
        //
        else if (trapPrefab.GetComponent<SpawnTrapBehaviour>())
        {
            DamageOptions[] damageOptions = trapPrefab.GetComponent<SpawnTrapBehaviour>().
        }
        //
        else
        {
            return "-";
        }
    }

    public string GetTrapTime()
    {
        if (trapPrefab && trapPrefab.GetComponent<IntervalTrapBehaviour>())
        {
            IntervalTrapBehaviour intervalTrapBehaviour = trapPrefab.GetComponent<IntervalTrapBehaviour>();

            if (intervalTrapBehaviour.intervalTime > 0)
            {
                return intervalTrapBehaviour.intervalTime.ToString() + "s";
            }
            else
            {
                return "-";
            }
        }
        else
        {
            return "-";
        }
    }

    public string GetTrapSpeed()
    {
        return "-";
    }
}
*/