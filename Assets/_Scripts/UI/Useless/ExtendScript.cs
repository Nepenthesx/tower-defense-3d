﻿/*
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ExtendScript : MonoBehaviour
{
    //Skrypt dołączany do pułapki

    //W tym obiekcie będzie przechowywane menu rozbudowy
    public GameObject extendMenuObjectPref;
    private GameObject extendMenuObject;

    //Z której strony jest ściana w danym Tile względem kamery (widza) | 0 - lewo, 1 - tył, 2 - prawo
    //Wzięte poprzednio z BuildScript
    [System.NonSerialized]
    public int wallDirection;

    //Czy daną pułapkę da się sprzedać bądź rozbudować (czy też inne opcje)
    public bool sellOption;
    public bool upgradeOption;

    public ButtonI cancelButton;
    public ButtonI sellButton;
    public ButtonI upgradeButton;
    private GameObject cancelButtonObject;
    private GameObject sellButtonObject;
    private GameObject upgradeButtonObject;

    //Mała ikonka z ceną (przy przycisku sprzedaży bądź ulepszania)
    public GameObject priceIconPrefab;
    //Względem przycisku (nie jest mnożone przez promień)
    public Vector3 priceIconPos;

    //Miejsce budowy (z przyciskiem)
    public GameObject buildingPlacePrefab;

    public GameObject framePrefab;
    public GameObject descriptionPrefab;
    public Vector3 descriptionPos;

    private GameObject description;

    //Odległość przycisków od wspólnego środka
    [System.NonSerialized]
    public float radius;

    private MoneyManager moneyManager;

    private Vector3 distanceToCamera;

    //Wzięte poprzednio z BuildScript
    [System.NonSerialized]
    public Vector3 defaultPosition;
    [System.NonSerialized]
    public Quaternion defaultRotation;

    //Opóźnienie po kliknięciu i zbudowaniu menu (By nie wcisnąć od razu anuluj)
    private float clickDelay;

    //Dwie zmienne dotyczące istnienia menu
    //BuildScript.menuExist - zmianna globalna, czy na mapie zostało już stworzone jakieś menu budowy (może być stworzone tylko jedno) - taka sama jak w BuildScript
    //myMenuExist - zmienna prywatne, czy menu jest stworzone przez mój obiekt
    private bool myMenuExist = false;

    void Start()
    {
        moneyManager = FindObjectOfType<MoneyManager>();

        //Pozycja w połowie między obiektem budowy a kamerą
        distanceToCamera = (transform.position + Camera.main.transform.position) / 2;
    }

    void Update()
    {
        //Jeśli pułapka została naciśnięta i globalne menu nie istnieje
        if (!BuildScript.menuExist && OnClick(transform))
        {
            BuildMenu();

            extendMenuObject.AddComponent<RotateToCamera>();

            BuildScript.menuExist = true;
            myMenuExist = true;
        }

        //Jeśli globalne menu istnieje i należy do mojego obiektu
        if (myMenuExist)
        {
            //Podświetlanie istniejących przycisków
            Highlight(cancelButtonObject, cancelButton);
            if (sellOption)
                Highlight(sellButtonObject, sellButton);
            if (upgradeOption)
            {
                //Jeśli na upgradeButtonObject zostanie najechany kursor
                if (OnMouse(upgradeButtonObject.transform))
                {
                    //Zmiana wyglądu - podświetlone
                    upgradeButtonObject.GetComponent<SpriteRenderer>().sprite = upgradeButton.highlightSprite;

                    //Tworzenie opisu ulepszonej pułapki, jeśli nie istnieje
                    if (!description)
                    {
                        description = Instantiate(descriptionPrefab, extendMenuObject.transform.position + descriptionPos * radius, extendMenuObject.transform.rotation, extendMenuObject.transform) as GameObject;
                        ChangeDescription(upgradeButton);
                    }
                }
                else
                {
                    //Zmiana wyglądu - normalne
                    upgradeButtonObject.GetComponent<SpriteRenderer>().sprite = upgradeButton.normalSprite;

                    //Usuwanie opisu jeśli istnieje
                    if (description)
                    {
                        Destroy(description);
                    }
                }
            }

            //Jeśli przycisk anulowania rozbudowy zostanie naciśnięty
            if (OnClick(cancelButtonObject.transform) && Time.time > clickDelay)
            {
                //Niszczy wszystkie przyciski (całe menu)
                Destroy(extendMenuObject);

                BuildScript.menuExist = false;
                myMenuExist = false;
            }

            //Jeśli przycisk sprzedaży zostanie naciśnięty
            if (sellOption && OnClick(sellButtonObject.transform))
            {
                //Tworzy z powrotem miejsce budowy
                GameObject buildingPlace = Instantiate(buildingPlacePrefab, defaultPosition, defaultRotation, GameObject.Find("BuildingPlaces").transform) as GameObject;
                buildingPlace.GetComponent<BuildScript>().wallDirection = wallDirection;

                BuildScript.menuExist = false;
                myMenuExist = false;

                //Dodanie pieniędzy
                EventsManager.current.SellTrap(sellButton.price);

                //Niszczy pułapkę
                Destroy(gameObject);
            }

            //Jeśli przycisk ulepszenia zostanie naciśnięty i liczba pieniędzy się zgadza
            if (upgradeOption && OnClick(upgradeButtonObject.transform) && moneyManager.GetMoney() >= upgradeButton.price)
            {
                //Tworzy ulepszoną pułapkę
                GameObject trap = Instantiate(upgradeButton.trapPrefab, defaultPosition + upgradeButton.trapPosition, RotatationByWallDirection()) as GameObject;
                trap.GetComponent<ExtendScript>().defaultPosition = defaultPosition;
                trap.GetComponent<ExtendScript>().defaultRotation = defaultRotation;
                trap.GetComponent<ExtendScript>().wallDirection = wallDirection;
                trap.GetComponent<ExtendScript>().radius = radius;

                BuildScript.menuExist = false;
                myMenuExist = false;

                //Usunięcie pieniędzy
                EventsManager.current.BuildTrap(upgradeButton.price);

                //Niszczy starą pułapkę
                Destroy(gameObject);
            }


        }
    }

    private bool OnClick(Transform hitTransform)
    {
        //Ray - promień poprowadzony tam gdzie pozycja kursora
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Jeśli gracz nacisnął LPM
        if (Input.GetMouseButtonDown(0))
        {
            //Jeśli został poprowadzony promień | out hit to informacja zwrotna o zderzeniu promienia z obiektem
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                //Jeśli obiekt, z którym zderzył się promień, jest konkretnym objektem (np mną)
                if (hit.transform == hitTransform)
                {
                    return true;
                }
            }
        }

        return false;
    }

    private bool OnMouse(Transform hitTransform)
    {
        //Ray - promień poprowadzony tam gdzie pozycja kursora
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Jeśli został poprowadzony promień | out hit to informacja zwrotna o zderzeniu promienia z obiektem
        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            //Jeśli obiekt, z którym zderzył się promień, jest konkretnym objektem (np mną)
            if (hit.transform == hitTransform)
            {
                return true;
            }
        }

        return false;
    }

    private void BuildMenu()
    {
        //Tworzy obiekt, w którym będą przechowywane przyciski
        extendMenuObject = Instantiate(extendMenuObjectPref, transform.position, Quaternion.identity, transform) as GameObject;

        //Zmiana położenia na połowę odległości do kamery
        extendMenuObject.transform.position = distanceToCamera;

        //Tworzy obramowanie i opis
        Instantiate(framePrefab, extendMenuObject.transform.position, Quaternion.identity, extendMenuObject.transform);

        //Tworzy przycisk anulowania
        Vector3 buttonPosC = extendMenuObject.transform.position + cancelButton.buttonPosition * radius;
        cancelButtonObject = Instantiate(cancelButton.buttonPrefab, buttonPosC, Quaternion.identity, extendMenuObject.transform) as GameObject;

        if (sellOption)
        {
            //Tworzy przycisk sprzedaży
            Vector3 buttonPosS = extendMenuObject.transform.position + sellButton.buttonPosition * radius;
            sellButtonObject = Instantiate(sellButton.buttonPrefab, buttonPosS, Quaternion.identity, extendMenuObject.transform) as GameObject;

            //Tworzy ikonkę z ceną
            GameObject priceIconSellObject = Instantiate(priceIconPrefab, sellButtonObject.transform.position + priceIconPos, Quaternion.identity, sellButtonObject.transform) as GameObject;
            priceIconSellObject.GetComponentInChildren<TextMeshPro>().text = sellButton.price.ToString();
        }

        if (upgradeOption)
        {
            //Tworzy przycisk ulepszenia
            Vector3 buttonPosU = extendMenuObject.transform.position + upgradeButton.buttonPosition * radius;
            upgradeButtonObject = Instantiate(upgradeButton.buttonPrefab, buttonPosU, Quaternion.identity, extendMenuObject.transform) as GameObject;

            //Tworzy ikonkę z ceną
            GameObject priceIconUpgradeObject = Instantiate(priceIconPrefab, upgradeButtonObject.transform.position + priceIconPos, Quaternion.identity, upgradeButtonObject.transform) as GameObject;
            priceIconUpgradeObject.GetComponentInChildren<TextMeshPro>().text = upgradeButton.price.ToString();
        }

        clickDelay = Time.time + 0.1f;
    }

    private void Highlight(GameObject buttonObject, ButtonI button)
    {
        //Jeśli na buttonObject zostanie najechany kursor
        if (OnMouse(buttonObject.transform))
        {
            //Zmiana wyglądu - podświetlone
            buttonObject.GetComponent<SpriteRenderer>().sprite = button.highlightSprite;
        }
        else
        {
            //Zmiana wyglądu - normalne
            buttonObject.GetComponent<SpriteRenderer>().sprite = button.normalSprite;
        }
    }

    private void ChangeDescription(ButtonI button)
    {
        //Podmienia odpwiednio napisy i ikony w opisie pułapki do budowania
        foreach (Transform child in description.transform)
        {
            switch (child.name)
            {
                case "NameText":
                    if (button.icon != null)
                        child.GetComponent<TextMeshPro>().text = button.name;
                    break;
                case "PriceText":
                    if (button.price != 0)
                        child.GetComponent<TextMeshPro>().text = button.price.ToString();
                    break;
                case "DamageText":
                    child.GetComponent<TextMeshPro>().text = button.GetTrapDamage();
                    break;
                case "TimeText":
                    child.GetComponent<TextMeshPro>().text = button.GetTrapTime();
                    break;
                case "SpeedText":
                    child.GetComponent<TextMeshPro>().text = button.GetTrapSpeed();
                    break;
                case "Icon":
                    if (button.icon != null)
                        child.GetComponent<SpriteRenderer>().sprite = button.icon;
                    break;
            }
        }
    }

    private void ClearDescription()
    {
        //Usuwa wszystkie napisy i ikony w opisie pułapki do budowania
        foreach (Transform child in description.transform)
        {
            switch (child.name)
            {
                case "NameText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "PriceText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "AttackText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "TimeText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "SpeedText":
                    child.GetComponent<TextMeshPro>().text = null;
                    break;
                case "Icon":
                    child.GetComponent<SpriteRenderer>().sprite = null;
                    break;
            }
        }

    }

    private Quaternion RotatationByWallDirection()
    {
        switch (wallDirection)
        {
            //lewo
            case 0:
                return Quaternion.Euler(defaultRotation.x, defaultRotation.y + 90, defaultRotation.z);
            //tył
            case 1:
                return Quaternion.Euler(defaultRotation.x, defaultRotation.y + 180, defaultRotation.z);
            //prawo
            case 2:
                return Quaternion.Euler(defaultRotation.x, defaultRotation.y + 270, defaultRotation.z);
            default:
                return defaultRotation;
        }
    }
}
*/