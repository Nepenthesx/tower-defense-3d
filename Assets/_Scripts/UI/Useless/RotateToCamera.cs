﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToCamera : MonoBehaviour
{
    //Obraca obiekt "przodem" do kamery

    void Update()
    {
        transform.rotation = Quaternion.LookRotation(new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, Camera.main.transform.position.z));
        transform.rotation = Quaternion.Euler(Camera.main.transform.eulerAngles.x, transform.rotation.y, transform.rotation.z);
    }
}
