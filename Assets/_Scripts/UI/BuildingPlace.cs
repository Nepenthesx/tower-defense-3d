﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingPlace : MonoBehaviour
{
    [SerializeField] private GameObject createBuildingMenuButtonPref;
    [SerializeField] private GameObject createExtensionMenuButtonPref;
    private GameObject createBuildingMenuButton = null;
    private GameObject createExtensionMenuButton = null;

    //Trudno jest znaleźć wzór na zależność między położeniem miejsca budowy a przyciskiem na Canvasie (wiele zależy od umiejscowienia kamery i samego poziomu)
    //Dla każdego miejsca budowy trzeba indywidualnie znaleźć właściwe miejsce przycisku budowy/rozbudowy
    [SerializeField] private Vector3 createBuildingMenuButtonPos;
    [SerializeField] private Vector3 createExtensionMenuButtonPos;

    [SerializeField] private GameObject buildingMenuPref;
    [SerializeField] private GameObject extensionMenuPref;

    public GameObject meleeDescriptionPref;
    public GameObject magicDescriptionPref;
    public GameObject supportDescriptionPref;
    public GameObject summonMeleeDescriptionPref;
    public GameObject summonMagicDescriptionPref;
    public Vector3 descriptionPos;

    //Pułapka (jeśli została zbudowana) która leży na miejscu BuildingPlace
    [HideInInspector] public GameObject trap;

    //Z której strony jest ściana na danym tile: 0 - po lewej, 1 - z tyłu, 2 - po prawej
    public int wallDirection;

    public static bool menuExist = false;

    //Czy pole jest zajęte przez wykonywanie akcji lub położeniem pułapki na nim (jeśli nie jest, to pojawia się przycisk budowy menu)
    [HideInInspector]
    public bool engaged = false;

    private Canvas canvas;


    void Start()
    {
        canvas = FindObjectOfType<Canvas>().GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        //Jeśli pole nie jest zajęte (brak stworzonego menu i budynku na nim) i nie istnieje przycisk tworzenia menu budowy
        if (!engaged && !createBuildingMenuButton && !menuExist)
        {
            //Generuje przycisk tworzenia menu budowy
            createBuildingMenuButton = Instantiate(createBuildingMenuButtonPref, createBuildingMenuButtonPos + new Vector3(960 - transform.position.x, 540, -transform.position.z), transform.rotation, canvas.transform) as GameObject;
            createBuildingMenuButton.name = "CreateBuildingMenuButton";

            //Nadaje mu opcję tworzenia menu budowy
            createBuildingMenuButton.GetComponent<Button>().onClick.AddListener(delegate{ CreateMenu(buildingMenuPref, createBuildingMenuButton); });
        }

        //Jeśli pole jest zajęte i nie istnieje przycisk tworzenia menu rozbudowy
        if (engaged && !createExtensionMenuButton && !menuExist)
        {
            //Generuje przycisk tworzenia menu rozbudowy
            createExtensionMenuButton = Instantiate(createExtensionMenuButtonPref, createExtensionMenuButtonPos + new Vector3(960 - transform.position.x, 540, -transform.position.z), transform.rotation, canvas.transform) as GameObject;
            createExtensionMenuButton.name = "CreateExtensionMenuButton";

            //Nadaje mu opcję tworzenia menu rozbudowy
            createExtensionMenuButton.GetComponent<Button>().onClick.AddListener(delegate{ CreateMenu(extensionMenuPref, createExtensionMenuButton); });
        }

        //Jeśli kursor znajduje się powyżej pewnej odległości od przycisku menu rozbudowy
        if (createExtensionMenuButton && Vector3.Distance(Input.mousePosition, createExtensionMenuButton.transform.position) > 200)
        {
            //Dezaktywuje przycisk (jest niewidoczny)
            createExtensionMenuButton.SetActive(false);
        }
        else if (createExtensionMenuButton)
        {
            createExtensionMenuButton.SetActive(true);
        }
    }



    public void CreateMenu(GameObject menuPref, GameObject createMenuButton)
    {
        if (!menuExist)
        {
            //Tworzy menu UI
            GameObject menu = Instantiate(menuPref, createMenuButton.transform.position, createMenuButton.transform.rotation, canvas.transform) as GameObject;

            menuExist = true;

            if (!engaged)
                engaged = true;

            //Nadaje odpowiednim przyciskom parametr buildingPlace
            foreach (Transform button in menu.transform)
            {
                if (button.GetComponent<CancelButton>())
                    button.GetComponent<CancelButton>().buildingPlace = GetComponent<BuildingPlace>();
                if (button.GetComponent<BuildTrapButton>())
                    button.GetComponent<BuildTrapButton>().buildingPlace = GetComponent<BuildingPlace>();
                if (button.GetComponent<SellTrapButton>())
                    button.GetComponent<SellTrapButton>().buildingPlace = GetComponent<BuildingPlace>();
            }

            menu.name = "Menu";

            //Niszczy przycisk tworzenia menu
            Destroy(createMenuButton.gameObject);
        }
    }

    public Quaternion RotatationByWallDirection()
    {
        switch (wallDirection)
        {
            //lewo
            case 0:
                return Quaternion.Euler(transform.rotation.x, transform.rotation.y - 90, transform.rotation.z);
            //tył
            case 1:
                return Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z);
            //prawo
            case 2:
                return Quaternion.Euler(transform.rotation.x, transform.rotation.y + 90, transform.rotation.z);
            default:
                return transform.rotation;
        }
    }
}
