﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CancelButton : MonoBehaviour
{
    [HideInInspector] public BuildingPlace buildingPlace;

    private void Update()
    {

    }

    public void CancelBuilding()
    {
        buildingPlace.engaged = false;
        BuildingPlace.menuExist = false;

        Destroy(GameObject.Find("Menu").gameObject);
    }

    public void CancelExtension()
    {
        BuildingPlace.menuExist = false;
        Destroy(GameObject.Find("Menu").gameObject);
    }
}
