﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EnemyInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [HideInInspector] public GameObject showInfoButton;

    //Statystyki (mogą być brane z prefabu przeciwnika)
    [SerializeField] private int health;
    [SerializeField] private int damage;
    [SerializeField] private string armor;
    [SerializeField] private string speed;

    private bool isPointer;
    
    void Start()
    {
        foreach (Transform child in transform)
        {
            switch (child.name)
            {
                case "Health":
                    child.GetComponent<Text>().text = health.ToString();
                    break;
                case "Damage":
                    child.GetComponent<Text>().text = damage.ToString();
                    break;
                case "Armor":
                    child.GetComponent<Text>().text = armor;
                    break;
                case "Speed":
                    child.GetComponent<Text>().text = speed;
                    break;
            }
        }
    }

    private void Update()
    {
        /* Pojawił się przycisk GotIt
         * 
        //Jeśli LPM zostanie naciśnięty gdy kursor jest poza obszarem informacji
        if (!isPointer)
        {   
            if (Input.GetMouseButtonDown(0))
            {
                //Przywraca realny czas gry
                Time.timeScale = 1;
                ShowInfoButton.isPaused = false;

                //Usuwa przycisk tworzenia informacji i samą iformację
                Destroy(showInfoButton.gameObject);
                Destroy(gameObject);
            }
        }
        */
    }

    public void GotIt()
    {
        //Przywraca realny czas gry
        Time.timeScale = 1;
        ShowInfoButton.isPaused = false;

        //Usuwa przycisk tworzenia informacji i samą iformację
        Destroy(showInfoButton.gameObject);
        Destroy(gameObject);
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        isPointer = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isPointer = false;
    }
}
