﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInfoButton : MonoBehaviour
{
    public GameObject infoPref;
    public static bool isPaused;

    private Canvas canvas;
   
    void Start()
    {
        canvas = FindObjectOfType<Canvas>().GetComponent<Canvas>();
    }

    public void ShowInfo()
    {
        //Tworzy informację
        GameObject info = Instantiate(infoPref, new Vector3(960, 540, 0), Quaternion.identity, canvas.transform) as GameObject;
        info.GetComponent<EnemyInfo>().showInfoButton = gameObject;

        //Zatrzymuje grę
        Time.timeScale = 0;
        isPaused = true;
    }
}
