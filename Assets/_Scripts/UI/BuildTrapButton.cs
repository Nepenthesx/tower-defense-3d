﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BuildTrapButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [HideInInspector] public BuildingPlace buildingPlace;

    [SerializeField] private GameObject trapPref;
    [SerializeField] private Vector3 trapPos;
    [SerializeField] private GameObject pricePreviewPref;
    [SerializeField] private Vector3 pricePreviewPos;

    private GameObject descriptionPref;

    private void Start()
    {
        //Jeśli pułapki nie można ulepszyć (opcja w TrapStatistics) to zniszcz obiekt (przycisk ulepszania)
        if (CompareTag("UpgradeButton") && !buildingPlace.trap.GetComponent<TrapStatistics>().canUpgrade)
        {
            Destroy(gameObject);
        }

        GameObject pricePreview = Instantiate(pricePreviewPref, transform.position + pricePreviewPos, Quaternion.identity, transform) as GameObject;
        pricePreview.GetComponentInChildren<Text>().text = trapPref.GetComponent<TrapStatistics>().buildPrice.ToString();
    }


    public void BuildTrap()
    {
        buildingPlace.trap = Instantiate(trapPref, buildingPlace.transform.position + trapPos, buildingPlace.RotatationByWallDirection()) as GameObject;

        EventsManager.current.BuildTrap(trapPref.GetComponent<TrapStatistics>().buildPrice);
        BuildingPlace.menuExist = false;


        Destroy(GameObject.Find("Menu"));
    }

    public void UpgradeTrap()
    {
        //Jak na razie nie ma potrzeby
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Przypisuje odpowiedni opis pułapki (w zależności od jej rodzaju)
        switch (trapPref.GetComponent<TrapStatistics>().type)
        {
            case TrapStatistics.TrapType.Melee:
                descriptionPref = buildingPlace.meleeDescriptionPref;
                break;
            case TrapStatistics.TrapType.Magic:
                descriptionPref = buildingPlace.magicDescriptionPref;
                break;
            case TrapStatistics.TrapType.Support:
                descriptionPref = buildingPlace.supportDescriptionPref;
                break;
            case TrapStatistics.TrapType.SummonMelee:
                descriptionPref = buildingPlace.summonMeleeDescriptionPref;
                break;
            case TrapStatistics.TrapType.SummonMagic:
                descriptionPref = buildingPlace.summonMagicDescriptionPref;
                break;
        }

        //Tworzy opis pułapki
        GameObject description = Instantiate(descriptionPref, GameObject.Find("Menu").transform.position + buildingPlace.descriptionPos, Quaternion.identity, GameObject.Find("Menu").transform) as GameObject;
        description.name = "TrapDescription";

        //Zmieniają się wartości (statystyki pułapki) w opisie
        foreach (Transform element in description.transform)
        {
            switch (element.name)
            {
                case "Icon":
                    element.GetComponent<Image>().sprite = trapPref.GetComponent<TrapStatistics>().icon;
                    break;
                case "Name":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().name;
                    break;
                case "BuildPrice":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().buildPrice.ToString();
                    break;
                case "Damage":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().damage.ToString();
                    break;
                case "ActionTime":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().actionTime.ToString();
                    break;
                case "RenewalTime":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().renewalTime.ToString();
                    break;
                case "Effect":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().effect;
                    break;
                case "Health":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().health.ToString();
                    break;
                case "Speed":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().speed.ToString();
                    break;
                case "Count":
                    element.GetComponent<Text>().text = trapPref.GetComponent<TrapStatistics>().count.ToString();
                    break;

            }
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Kasuje się opis pułapki
        if (GameObject.Find("TrapDescription"))
        {
            Destroy(GameObject.Find("TrapDescription").gameObject);
        }
        
    }
}
