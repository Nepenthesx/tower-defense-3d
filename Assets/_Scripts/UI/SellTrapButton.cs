﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellTrapButton : MonoBehaviour
{
    [HideInInspector] public BuildingPlace buildingPlace;

    [SerializeField] private GameObject pricePreviewPref;
    [SerializeField] private Vector3 pricePreviewPos;

    private void Start()
    {
        //Jeśli pułapki nie można sprzedać (opcja w TrapStatistics) to zniszcz obiekt (przycisk sprzedaży)
        if (CompareTag("SellButton") && !buildingPlace.trap.GetComponent<TrapStatistics>().canSell)
        {
            Destroy(gameObject);
        }

        GameObject pricePreview = Instantiate(pricePreviewPref, transform.position + pricePreviewPos, Quaternion.identity, transform) as GameObject;
        pricePreview.GetComponentInChildren<Text>().text = buildingPlace.trap.GetComponent<TrapStatistics>().sellPrice.ToString();
    }

    public void SellTrap()
    {
        buildingPlace.engaged = false;
        BuildingPlace.menuExist = false;

        EventsManager.current.SellTrap(buildingPlace.trap.GetComponent<TrapStatistics>().sellPrice);
        Destroy(buildingPlace.trap.gameObject);

        Destroy(GameObject.Find("Menu").gameObject);
    }
}
