﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow1 : MonoBehaviour
{
    public GameObject aim;
    public float speed;

    private Vector3 deltaRotation;
    private float maxDeltaHeight;

    void Start()
    {
        maxDeltaHeight = transform.position.y;
    }


    void Update()
    {
        //ruch
        transform.position = Vector3.MoveTowards(transform.position, aim.transform.position, speed * Time.deltaTime);

        //obrót x i a
        deltaRotation = aim.transform.position - transform.position;
        deltaRotation.y = 90f;
        transform.rotation = Quaternion.LookRotation(deltaRotation);

        //obrót y
        transform.rotation = Quaternion.Euler(90 + transform.position.y * 180 / maxDeltaHeight, transform.rotation.y, transform.rotation.z);
        print(transform.position.y * 180 / maxDeltaHeight - 90);
    }
}
