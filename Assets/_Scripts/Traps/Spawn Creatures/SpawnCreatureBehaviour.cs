﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class SpawnCreatureBehaviour : MonoBehaviour
//{
//    [Header("Attack options")]
//    [SerializeField]
//    private DamageOptions[] attackOptions;
//    public float attackInterval = 2f;

//    [Header("Health options")]
//    public float maxHealth;
//    private float health;
//    [SerializeField]
//    private DamageOptions[] vulnerabilities;

//    [Header("Other")]
//    public Transform stationingArea;
//    public float speed = 1f;

//    private GameObject enemy;
//    private Animator _anim;

//    private CharacterState state;

//    private Coroutine fightingCoroutine;
//    private Coroutine patrollingCoroutine;

//    public bool IsDead() { return state == CharacterState.Dead; }

//    void Start()
//    {
//        gameObject.name = gameObject.name + Random.Range(0, 100);
//        _anim = GetComponent<Animator>();
//        health = maxHealth;
//        state = CharacterState.Idle;
//    }

//    void Update()
//    {
//        CheckIfShouldBeDead();
//        if (stationingArea == null || IsDead()) { return; }
//        SetNewEnemyIfPossible();
//        PatrolIfPossible();
//    }

//    public void ApplyDamage(DamageOptions[] damageOptionsList)
//    {
//        if (IsDead()) { return; }

//        float totalDamageTaken = 0;
//        for (int i = 0; i < damageOptionsList.Length; i++)
//        {
//            DamageOptions currentDamageOption = damageOptionsList[i];
//            float finalDamageTaken = currentDamageOption.power * System.Array.Find(vulnerabilities, option => option.type == currentDamageOption.type).power;
//            totalDamageTaken += finalDamageTaken;
//        }
//        health -= totalDamageTaken;
//    }

//    public CharacterState GetState()
//    {
//        return state;
//    }

//    public float GetHealth()
//    {
//        return health;
//    }

//    public float GetMaxHealth()
//    {
//        return maxHealth;
//    }

//    private void SetNewEnemyIfPossible()
//    {
//        if (enemy != null) { return; }

//        // Wykrywa obiekty wokół bazy, z której wyszedł
//        Collider[] colliders = Physics.OverlapBox(stationingArea.position + Vector3.up / 2, Vector3.one / 2);

//        for (int i = 0; i < colliders.Length; i++)
//        {
//            // jeśli jeden z tych obiektów jest wrogiem, który nie jest martwy ani nie walczy, to zaczyna go atakować
//            if (colliders[i].tag == "Enemy")
//            {
//                GameObject currentEnemy = colliders[i].gameObject;
//                CharacterState enemyState = currentEnemy.GetComponent<EnemyBehaviour>().GetState();
//                if (enemyState != CharacterState.Dead && enemyState != CharacterState.Fighting)
//                {
//                    enemy = currentEnemy;
//                    state = CharacterState.Fighting;
//                    enemy.GetComponent<EnemyBehaviour>().Fight(gameObject);
//                    RotateTo(enemy.transform.position);
//                    fightingCoroutine = StartCoroutine(ContinueGivingDamage());
//                    return;
//                }
//            }
//        }
//    }

//    private void PatrolIfPossible()
//    {
//        if (patrollingCoroutine != null || fightingCoroutine != null) { return; }

//        patrollingCoroutine = StartCoroutine(Patrol());
//    }

//    private void RotateTo(Vector3 rotationPoint)
//    {
//        transform.LookAt(new Vector3(rotationPoint.x, transform.position.y, rotationPoint.z));
//    }

//    private IEnumerator ContinueGivingDamage()
//    {
//        while (enemy != null && !enemy.GetComponent<EnemyLife>().GetIsDead())
//        {
//            state = CharacterState.Fighting;
//            enemy.BroadcastMessage("ApplyDamage", attackOptions);
//            yield return new WaitForSeconds(attackInterval);
//        }
//        state = CharacterState.Idle;
//        enemy = null;
//        fightingCoroutine = null;
//    }

//    private IEnumerator Patrol()
//    {
//        while (enemy == null)
//        {
//            state = CharacterState.Walking;
//            Vector3 pointToGo = stationingArea.position + new Vector3(Random.Range(-0.5f, 0.5f), 0, Random.Range(-0.5f, 0.5f));
//            RotateTo(pointToGo);
//            Vector3 currentXZPosition = new Vector3(transform.position.x, 0, transform.position.z);
//            while (enemy == null && currentXZPosition != pointToGo)
//            {
//                MoveTowards(pointToGo);
//                currentXZPosition = new Vector3(transform.position.x, 0, transform.position.z);

//                yield return null;
//            }
//            state = CharacterState.Idle;
//            yield return new WaitForSeconds(2f);
//        }
//        patrollingCoroutine = null;
//    }

//    private IEnumerator GoTo(Vector3 directionPoint)
//    {
//        Vector3 currentXZPosition = new Vector3(transform.position.x, 0, transform.position.z);
//        while (currentXZPosition != directionPoint)
//        {
//            MoveTowards(directionPoint);
//            currentXZPosition = new Vector3(transform.position.x, 0, transform.position.z);
//            yield return null;
//        }
//    }

//    private void MoveTowards(Vector3 directionPoint)
//    {
//        transform.position = Vector3.MoveTowards(transform.position, directionPoint, speed * Time.deltaTime);
//    }

//    private void CheckIfShouldBeDead()
//    {
//        if (!IsDead() && health <= 0)
//        {
//            Die();
//        }
//    }

//    private void Die()
//    {
//        state = CharacterState.Dead;
//        StopAllCoroutines();
//        StartCoroutine(DestroyAfterDeathAnimation());
//    }

//    private IEnumerator DestroyAfterDeathAnimation()
//    {
//        yield return new WaitForSeconds(1f);
//        Destroy(gameObject);
//    }
//}
