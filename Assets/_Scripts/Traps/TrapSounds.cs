﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class TrapSounds : MonoBehaviour
{
    [SerializeField] private Vector2 pitchModifierRange;
    [SerializeField] private Vector2 volumeModifierRange;
    
    public Sound[] sounds;

    public void PlaySoundWithRandomModifications(int index)
    {
        if (index > sounds.Length - 1)
            return;

        Sound selected = sounds[index];
        PlaySound(ref selected);
    }
    public void PlaySoundWithRandomModifications(string name)
    {
        Sound selected = Array.Find(sounds, sound => sound.name == name);
        if (selected == null)
            return;
        
        PlaySound(ref selected);
    }

    private void PlaySound(ref Sound selected)
    {
        selected.source.pitch = selected.pitch * Random.Range(pitchModifierRange.x, pitchModifierRange.y);
        selected.source.volume = selected.pitch * Random.Range(volumeModifierRange.x, volumeModifierRange.y) * FindObjectOfType<OptionsManager>().GetComponent<OptionsManager>().soundVolume;
        selected.source.Play();
    }
    
    private void Start()
    {
        foreach (Sound sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;

            sound.source.volume = FindObjectOfType<OptionsManager>().GetComponent<OptionsManager>().soundVolume;
            sound.source.pitch = sound.pitch;

            sound.source.loop = sound.loop;
            sound.source.playOnAwake = false;
        }
    }
}
