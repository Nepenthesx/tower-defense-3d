﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTrapBehaviour : MonoBehaviour
{
    public float radius = 0.3f;
    public CharacterType allyToSpawn;
    [SerializeField]
    private int maxNumberOfCreatures;
    [SerializeField]
    private float respawnTime;
    [SerializeField]
    private ParticleSystem particle;

    private bool disabled;
    private float disabilityTimer;
    private bool spawningInProgress = false;

    private GameObject[] spawnedCreatures;

    public void DisableTrap(float time)
    {
        disabled = true;
        disabilityTimer = time;
    }

    private void Start()
    {
        spawnedCreatures = new GameObject[maxNumberOfCreatures];
        particle.Stop();
        particle.Clear();
    }

    private void Update()
    {
        if (disabled && disabilityTimer > 0)
        {
            disabilityTimer -= Time.deltaTime;
            return;
        }
        
        for (int i = 0; i < maxNumberOfCreatures; i++)
        {
            if (spawnedCreatures[i] == null && spawningInProgress == false)
            {
                this.spawningInProgress = true;
                StartCoroutine(spawnCreature(i));
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Character character;
        if (!other.gameObject.TryGetComponent(out character) || character.IsDead()) { return; }
        foreach (GameObject creature in spawnedCreatures)
        {
            if (creature != null)
            {
                EventsManager.current.FightWith(creature.GetInstanceID(), other.gameObject);
            }
        }
    }

    IEnumerator spawnCreature(int index)
    {
        yield return new WaitForSeconds(respawnTime);
        float angle = (float)index / (float)maxNumberOfCreatures * 2f * Mathf.PI - 0.5f * Mathf.PI;
        float xPos = Mathf.Sin(angle);
        float zPos = Mathf.Cos(angle);
        Vector3 offset = new Vector3(xPos, 0, zPos) * radius;
        GameObject newCreature = GameObject.Instantiate(allyToSpawn.prefab, transform.position + offset, Quaternion.identity);
        particle.Stop();
        particle.Play();
        Ally allyComponent = newCreature.GetComponent<Ally>();
        allyComponent.SetStationingPoint(transform.position);
        allyComponent.SetInitials(allyToSpawn);
        this.spawnedCreatures[index] = newCreature;
        this.spawningInProgress = false;
    }
}
