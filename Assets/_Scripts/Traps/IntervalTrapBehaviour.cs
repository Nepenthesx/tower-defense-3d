﻿using System.Collections.Generic;
using UnityEngine;

public class IntervalTrapBehaviour : MonoBehaviour
{
    public float intervalTime = 3f;
    public float damageTime;

    public DamageOptions[] damageOptions = new DamageOptions[0];
    public CharacterStatsEffect[] effects = new CharacterStatsEffect[0];

    private bool disabled;
    private float disabilityTimer;
    private float timePassed;
    private HitAreaCollidersMonitor hitAreaMonitor;
    private Animator anim;
    private TrapSounds soundsController;


    public void DisableTrap(float time)
    {
        disabled = true;
        disabilityTimer = time;
    }

    private void Start()
    {
        hitAreaMonitor = GetComponentInChildren<HitAreaCollidersMonitor>();
        anim = GetComponent<Animator>();
        soundsController = GetComponent<TrapSounds>();
    }

    private void Update()
    {
        if (disabled && disabilityTimer > 0)
        {
            disabilityTimer -= Time.deltaTime;
            return;
        }

        disabled = false;
        timePassed += Time.deltaTime;
        if (timePassed > intervalTime)
        {
            anim.SetBool("active", true);
            PlaySound();
            PerformActionsWithinHitArea();
            timePassed = 0f;
        }
        else if (damageTime > 0 && timePassed < damageTime)
        {
            PerformActionsWithinHitArea();
        }
        else
        {
            anim.SetBool("active", false);
        }
    }

    private void PerformActionsWithinHitArea()
    {
        List<Collider> collidersInRange = hitAreaMonitor.GetColliders();

        foreach (Collider collider in collidersInRange)
        {
            if (collider != null)
            {
                GiveDamage(collider);
                GiveEffects(collider);
            }
        }
    }

    private void GiveDamage(Collider collider)
    {
        if (damageOptions.Length == 0) return;
        if (damageTime > 0) // obrazenia rozlozone w czasie
        {
            float damageMultiplier = Time.deltaTime / damageTime;
            damageMultiplier =
                damageMultiplier > 1
                    ? 1
                    : damageMultiplier; // zabezpieczenie na wypadek przycinania sie gry i zadawania za duzych obrazen przez zbyt niski fps

            DamageOptions[] multipliedDamage = new DamageOptions[damageOptions.Length];
            for (int j = 0; j < multipliedDamage.Length; j++)
            {
                multipliedDamage[j] = DamageOptions.MultiplyBy(damageOptions[j], damageMultiplier);
            }

            ApplyDamage(collider.gameObject.GetInstanceID(), multipliedDamage);
        }
        else
        {
            ApplyDamage(collider.gameObject.GetInstanceID(), damageOptions);
        }
    }

    private void GiveEffects(Collider collider)
    {
        if (effects.Length == 0) return;
        foreach (CharacterStatsEffect effect in effects)
        {
            EventsManager.current.ApplyStatsEffect(collider.gameObject.GetInstanceID(), effect);
        }
    }

    private void ApplyDamage(int id, DamageOptions[] damageOptions)
    {
        EventsManager.current.ApplyDamage(id, damageOptions);
    }

    private void PlaySound()
    {
        soundsController.PlaySoundWithRandomModifications(0);
    }
}