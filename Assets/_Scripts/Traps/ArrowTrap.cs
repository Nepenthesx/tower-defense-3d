﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTrap : MonoBehaviour
{
    public GameObject arrowPrefab;
    public string turn;

    private GameObject newArrow;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        if (turn == "south")
            newArrow = Instantiate(arrowPrefab, new Vector3(transform.position.x + 0.68f, transform.position.y + 0.25f, transform.position.z - 0.25f), Quaternion.Euler(-90, 0, 0), transform) as GameObject;

        newArrow.GetComponent<Arrow>().turn = turn;
    }
}
