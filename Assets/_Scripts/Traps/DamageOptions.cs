﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class that serves as an indicator for power and type of damage that traps do but also as a describer of heroes vulnerabilities.
/// When it comes to the damage power of a trap it should be [0 - inf). Heroes vulnerabilities should stay in [0-1].
/// </summary>
[System.Serializable]
public class DamageOptions
{
    public DamageType type;
    public float power;

    public DamageOptions(DamageType damageType)
    {
        type = damageType;
        power = 1;
    }

    public DamageOptions(DamageType damageType, float damagePower)
    {
        type = damageType;
        power = damagePower;
    }

    public static DamageOptions MultiplyBy(DamageOptions damageOptions, float multiplier)
    {
        return new DamageOptions(damageOptions.type, multiplier * damageOptions.power);
    }
}

public enum DamageType
{
    Melee,
    Magic
}
