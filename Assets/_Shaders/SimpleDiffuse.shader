﻿Shader "Custom/SimpleDiffuse"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
		_OutlineColor ("Outline Color", Color) = (1,1,1,1)
		_DotProduct ("Outline threshold", Range(-1, 1)) = 0.25
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        #pragma surface surf Lambert fullforwardshadows

        #pragma target 3.0

        struct Input
        {
			float3 viewDir;
			float3 worldNormal;
        };

        fixed4 _Color;
		fixed4 _OutlineColor;
		fixed _DotProduct;

		void surf(Input IN, inout SurfaceOutput o)
		{
			float border = abs(dot(IN.viewDir, o.Normal));
			if (_DotProduct < border) {
				o.Albedo = _Color.rgb;
			}
			else {
				o.Albedo = _OutlineColor.rgb;
			}
		}
		ENDCG
    }
    FallBack "Diffuse"
}
